using StarterAssets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class AnimalCharacterAI : MonoBehaviour
{
    public GameObject elderBrotherCharacter, youngerBrotherCharacter;
    public GameObject itemHolder;
    public PlayerInput elderBrotherPlayerInput, youngerBrotherPlayerInput;
    public Animator _animator;
    public ParticleSystem ankaEffect;
    public float moveSpeed = 2f;
    public float waitToTakeItemSeconds = 1f;
    public float waitToReturnSeconds = 1f;
    public float waitToDropDownItemSeconds = 1f;
    public float waitToBackToNormalSeconds = 1f;
    public float followPlayerOffsetHeight = 5f;
    public float followPlayerStoppingDistance = 2f;
    public float followPlayerStoppingDistanceOffset = 1f;
    public float pickItemStoppingDistance = 0.5f;
    public float faceTargetPlayerRotationSpeed = 1.0f;
    public float itemOffsetY = 0.1f, trapOffsetY = 0.1f;
    public float getItemDistance = 0.45f;
    public float goThroughDuration = 1f, goThroughOffSetY = 1.5f;
    public float goThroughHeight = 10f;
    public float pickItemMoveDownSpeed = 0.008f, activateTrapMoveDownSpeed = 0.01f;
    public float pickItemMoveUpSpeed = 0.008f, activateTrapMoveUpSpeed = 0.01f;
    public float attackEnemySpeed = 1.2f;
    public float returnToPlayerPosDistance = 0.5f;
    public float endTrapDistance = 0.95f;
    public float destroyAnkaEffectSeconds = 3f;
    public Transform dropItemPosition;

    [SerializeField] GameObject targetItem, targetTrap, targetEnemy;
    [SerializeField] TrapTrigger targetTrapTrigger;
    private Transform getItemCheckPoint, getItemPlacePosition;
    private static Vector3 beforeAttractEnemiesPosition;
    private GameObject targetPlayer;
    private Vector3 pickUpItemPosition;
    [SerializeField] Vector3 returnPos;
    private NavMeshAgent navMeshAgent;
    private Rigidbody rb;
    [SerializeField] Vector3 attractEnemyPosition, attractEnemyFacePosition;
    private CapsuleCollider capsuleCollider;
    private float trapDistance;
    private bool _hasAnimator;

    private enum AIState {
        Normal,
        TakeItem,
        StartPickUp,
        PickedUp,
        PickItemReturn,
        DropDownItem,
        PrepareToActivateTrap,
        StartToActivateTrap,
        AcitivateTrapReturn,
        AttackEnemy,
        EndOfAttackingEnemy,
        AttractEnemy,
        EndOfAttractingEnemy
    };
    private AIState currentAIState;
    private Vector3 _startPos, _currentPos;
    private Vector3 _velocity = Vector3.zero;
    private float _startTime;
    private float _rotationVelocity;
    private bool _itemPicked;
    private static Vector3 activateTrapReturnPlace;
    private static bool m_isAttractingEnemy;

    // animation IDs
    private int _animIDSpeed;
    private int _animIDMotionSpeed;

    // Start is called before the first frame update
    void Awake()
    {
        Initialization();
    }

    private void Initialization()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.autoTraverseOffMeshLink = false;
        navMeshAgent.stoppingDistance = followPlayerStoppingDistance;
        navMeshAgent.speed = moveSpeed;
        _hasAnimator = TryGetComponent(out _animator);
        _animIDSpeed = Animator.StringToHash("Speed");
        _animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
        rb = GetComponent<Rigidbody>();
        currentAIState = AIState.Normal;
        _startTime = Time.time;
        targetPlayer = youngerBrotherCharacter;
        pickUpItemPosition = Vector3.zero;
        _itemPicked = false;
        targetEnemy = null;
        targetTrapTrigger = null;
        m_isAttractingEnemy = false;
        capsuleCollider = GetComponent<CapsuleCollider>();
        beforeAttractEnemiesPosition = Vector3.zero;
        returnPos = Vector3.zero;
        trapDistance = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (targetPlayer == null)
        {
            return;
        }

        switch (currentAIState)
        {
            case AIState.Normal:
                FollowTargetPlayer();
                GoThroughBridge();
                break;
            case AIState.TakeItem:
                if (itemHolder == null) { return; }
                StartToTakeItem();
                break;
            case AIState.StartPickUp:
                StartPicking();
                break;
            case AIState.PickedUp:
                PickUpItem();
                break;
            case AIState.PickItemReturn:
                StartCoroutine(PickItemReturn());
                break;
            case AIState.DropDownItem:
                StartCoroutine(DropDownItem());
                break;
            case AIState.PrepareToActivateTrap:
                GoToTheTrap();
                break;
            case AIState.StartToActivateTrap:
                StartCoroutine(StartFreeFallingTrap());
                break;
            case AIState.AcitivateTrapReturn:
                EndOfActivatingTrap();
                break;
            case AIState.AttackEnemy:
                StartToAttackEnemy();
                break;
            case AIState.EndOfAttackingEnemy:
                StartToReturnAfterAttackingEnemy();
                break;
            case AIState.AttractEnemy:
                AttractEnemies();
                break;
            case AIState.EndOfAttractingEnemy:
                ReturningFromAttractingEnemies();
                break;
        }
    }
    private void FaceTarget(Vector3 destination)
    {
        Vector3 lookPos = destination - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, faceTargetPlayerRotationSpeed);
    }

    private void FollowTargetPlayer()
    {
        if (navMeshAgent.isActiveAndEnabled)
        {
            navMeshAgent.SetDestination(targetPlayer.transform.position);
            navMeshAgent.stoppingDistance = followPlayerStoppingDistance;
            if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                navMeshAgent.isStopped = true;
                navMeshAgent.speed = 0f;
                rb.constraints = RigidbodyConstraints.FreezeAll;
                if (_hasAnimator)
                {
                    _animator.SetFloat(_animIDSpeed, 0f);
                    _animator.SetFloat(_animIDMotionSpeed, 0f);
                }
            }
            else
            {
                navMeshAgent.isStopped = false;
                navMeshAgent.speed = moveSpeed;
                rb.constraints = RigidbodyConstraints.FreezeRotation;
                if (_hasAnimator)
                {
                    _animator.SetFloat(_animIDSpeed, 1f);
                    _animator.SetFloat(_animIDMotionSpeed, 1f);
                }
            }
            FaceTarget(targetPlayer.transform.position);
        }
    }
    private void GoThroughBridge()
    {
        if (navMeshAgent.isOnOffMeshLink)
        {
            StartCoroutine(StartFlyThrough());
            navMeshAgent.CompleteOffMeshLink();
        }
    }

    private IEnumerator StartFlyThrough(string TargetAreaName="")
    {
        OffMeshLinkData data;
        Vector3 startPos = navMeshAgent.transform.position;
        Vector3 endPos;
        if (!TargetAreaName.Equals(""))
        {
            endPos = GameObject.Find(TargetAreaName).transform.position;
        }
        else
        {
            data = navMeshAgent.currentOffMeshLinkData;
            endPos = data.endPos;
        }

        float time = 0.0f;

        while (time < 1.0f)
        {
            navMeshAgent.transform.position = Vector3.Lerp(startPos, endPos + new Vector3(0f, goThroughOffSetY, 0f), time);

            time += Time.deltaTime / goThroughDuration;
            yield return null;
        }
    }

    public void SetTargetPlayer(string followCharacter)
    {
        switch (followCharacter)
        {
            case "ElderBrother":
                targetPlayer = elderBrotherCharacter;
                break;
            case "YoungerBrother":
                targetPlayer = youngerBrotherCharacter;
                break;
        }
    }

    public void SetTargetItem(GameObject targetItemObj)
    {
        Debug.Log("SetTargetItem: " + targetItemObj);
        targetItem = targetItemObj;
    }

    public void UnsetTargetItem()
    {
        targetItem = null;
    }

    public bool HasTargetItem()
    {
        return targetItem != null;
    }

    public void OrderToTakeItem()
    {
        if(getItemCheckPoint == null)
        {
            ChangeAIState(AIState.Normal);
        }

        navMeshAgent.enabled = false;
        _itemPicked = true;
        ChangeAIState(AIState.TakeItem);
    }

    private void ChangeAIState(AIState targetAIState)
    {
        currentAIState = targetAIState;
    }

    public void SetGetItemPointPosittion(Transform itemCheckPoint, Transform itemPlacePos, Transform dropItemPos)
    {
        getItemCheckPoint = itemCheckPoint;
        getItemPlacePosition = itemPlacePos;
        dropItemPosition = dropItemPos;
    }

    public void UnsetGetItemPointPosittion()
    {
        getItemCheckPoint = null;
        getItemPlacePosition = null;
        dropItemPosition = null;
    }

    private void UnsetGetItemPointPosition()
    {
        getItemCheckPoint = null;
    }

    private void StartToTakeItem()
    {
        if (targetItem.tag.Equals("Bottle"))
        {
            FindObjectOfType<GameManager>().PickedBottle();
        }
        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, targetItem.transform.position + new Vector3(0f, itemOffsetY, 0f), pickItemMoveUpSpeed);
        Debug.Log("Bird and Bottle Distance: "+ Vector3.Distance(gameObject.transform.position, targetItem.transform.position));
        if (Vector3.Distance(gameObject.transform.position, targetItem.transform.position) <= getItemDistance)
        {
            ChangeAIState(AIState.StartPickUp);
        }
    }

    private void StartPicking()
    {
        if (pickUpItemPosition == Vector3.zero)
        {
            pickUpItemPosition = gameObject.transform.position;
        }
        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, targetItem.transform.position + new Vector3(0f, itemOffsetY, 0f), pickItemMoveDownSpeed);
        if (gameObject.transform.position.y - targetItem.transform.position.y <= getItemDistance)
        {
            ChangeAIState(AIState.PickedUp);
        }
    }

    private void PickUpItem()
    {
        if (pickUpItemPosition == Vector3.zero) { return; }

        targetItem.GetComponent<Item>().DisableRagdoll();
        targetItem.transform.parent = itemHolder.transform;

        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, pickUpItemPosition, pickItemMoveUpSpeed);

        if (gameObject.transform.position.y - pickUpItemPosition.y <= 1f)
        {
            ChangeAIState(AIState.PickItemReturn);
        }
    }

    private IEnumerator PickItemReturn()
    {
        yield return new WaitForSeconds(waitToReturnSeconds);

        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, dropItemPosition.position, pickItemMoveDownSpeed);

        if (Vector3.Distance(gameObject.transform.position, dropItemPosition.transform.position) <= 0.25f)
        {
            ItemPickedToDestintion();
        }
    }

    public void ItemPickedToDestintion()
    {
        ChangeAIState(AIState.DropDownItem);
    }

    public bool IsItemPicked()
    {
        return _itemPicked;
    }

    private IEnumerator DropDownItem()
    {
        yield return new WaitForSeconds(waitToBackToNormalSeconds);

        targetItem.transform.SetParent(null);
        targetItem.GetComponent<Item>().EnableRagdoll();
        targetItem.transform.SetParent(GameObject.Find("Items").transform);

        UnsetGetItemPointPosition();

        navMeshAgent.enabled = true;
        navMeshAgent.SetDestination(targetPlayer.transform.position - new Vector3(0f, 0f, 1f));
        navMeshAgent.stoppingDistance = 0f;

        if (navMeshAgent.remainingDistance <= 0.2f)
        {
            ChangeAIState(AIState.Normal);
        }
    }

    public void SetTargetTrap(GameObject trap, TrapTrigger trapTrigger, float trapOffsetY = 1f, float trapDistance = 0.25f)
    {
        if (!trap.GetComponent<FreeFallTrap>().IsTrapActivated())
        {
            targetTrap = trap;
            targetTrapTrigger = trapTrigger;
            this.trapOffsetY = trapOffsetY;
            this.trapDistance = trapDistance;
        }
    }

    public void OrderToActivateTrap()
    {
        if (targetTrap != null)
        {
            returnPos = gameObject.transform.position - new Vector3(0f, 0f, 1f);
            activateTrapReturnPlace = gameObject.transform.position;
            navMeshAgent.enabled = false;
            ChangeAIState(AIState.PrepareToActivateTrap);
            FindObjectOfType<ThirdPersonController>().DisableActivateTrap();
        }
    }

    private void GoToTheTrap()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
        {
            elderBrotherPlayerInput.enabled = false;
        }
        else
        {
            youngerBrotherPlayerInput.enabled = false;
        }

        FaceTarget(targetTrap.transform.position);

        _animator.SetFloat(_animIDSpeed, 1f);
        _animator.SetFloat(_animIDMotionSpeed, 1f);

        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, targetTrap.transform.position + new Vector3(0f, trapOffsetY, 0f), activateTrapMoveUpSpeed);
        //Debug.Log("Vector3.Distance(gameObject.transform.position, targetTrap.transform.position): " + Vector3.Distance(gameObject.transform.position, targetTrap.transform.position));
        if (Vector3.Distance(gameObject.transform.position, targetTrap.transform.position) <= trapDistance)
        {
            _animator.SetFloat(_animIDSpeed, 0f);
            _animator.SetFloat(_animIDMotionSpeed, 0f);
            ChangeAIState(AIState.StartToActivateTrap);
        }
    }

    private IEnumerator StartFreeFallingTrap()
    {
        if (targetTrap != null)
        {
            ParticleSystem newAnkaEffect = Instantiate(ankaEffect);
            newAnkaEffect.transform.position = gameObject.transform.position;
            newAnkaEffect.transform.rotation = Quaternion.identity;
            newAnkaEffect.Play();
            Destroy(newAnkaEffect, destroyAnkaEffectSeconds);

            _animator.SetTrigger("ActivateTrap");
            targetTrap.GetComponent<FreeFallTrap>().ActivateFreeFallTrap();
            targetTrapTrigger.TrapActivated();
            targetTrap = null;
            _animator.SetTrigger("ReturnFromTrap");
        }

        yield return new WaitForSeconds(waitToBackToNormalSeconds);

        ChangeAIState(AIState.AcitivateTrapReturn);
    }

    private void EndOfActivatingTrap()
    {
        FaceTarget(activateTrapReturnPlace);
        _animator.SetFloat(_animIDSpeed, 1f);
        _animator.SetFloat(_animIDMotionSpeed, 1f);

        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, activateTrapReturnPlace - new Vector3(0f, 0f, 1f), activateTrapMoveDownSpeed);
        if (Vector3.Distance(gameObject.transform.position, activateTrapReturnPlace) <= endTrapDistance)
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                elderBrotherPlayerInput.enabled = true;
            }
            else
            {
                youngerBrotherPlayerInput.enabled = true;
            }
            StartCoroutine(ReturnToTargetPlayerPos());
        }
    }

    public void SetEndTrapDistance(float endTrapDistance)
    {
        this.endTrapDistance = endTrapDistance;
    }

    private IEnumerator ReturnToTargetPlayerPos()
    {
        yield return null;

        //Debug.Log("returnPos: "+ returnPos);

        FaceTarget(returnPos);

        DisableCollisionSystem();
        transform.position = Vector3.MoveTowards(transform.position, returnPos, attackEnemySpeed * 5 * Time.deltaTime);
        _animator.SetFloat(_animIDSpeed, 1f);
        _animator.SetFloat(_animIDMotionSpeed, 1f);

        if (Vector3.Distance(transform.position, returnPos) <= returnToPlayerPosDistance)
        {
            navMeshAgent.enabled = true;
            navMeshAgent.speed = 0f;
            navMeshAgent.isStopped = true;
            _animator.SetFloat(_animIDSpeed, 0f);
            _animator.SetFloat(_animIDMotionSpeed, 0f);
            EnableCollisionSystem();

            ChangeAIState(AIState.Normal);
        }
    }

    public void SetActiviateTrapMoveSpeed(float moveUpSpeed, float moveDownSpeed)
    {
        activateTrapMoveUpSpeed = moveUpSpeed;
        activateTrapMoveDownSpeed = moveDownSpeed;
    }

    public void SetReturnToPlayerPosDistance(float returnToPlayerPosDistance)
    {
        this.returnToPlayerPosDistance = returnToPlayerPosDistance;
    }

    public void SetTargetEnemy(GameObject enemy)
    {
        targetEnemy = enemy;
    }

    public void UnsetTargetEnemy()
    {
        targetEnemy = null;
    }

    public void AttackEnemy()
    {
        if (targetEnemy == null) { return; }

        returnPos = gameObject.transform.position - new Vector3(0f, 0f, 1f);

        ChangeAIState(AIState.AttackEnemy);
    }

    private void StartToAttackEnemy()
    {
        if(targetEnemy != null)
        {
            FaceTarget(targetEnemy.transform.position);

            navMeshAgent.enabled = false;

            transform.position = Vector3.MoveTowards(transform.position, targetEnemy.transform.position + new Vector3(0f, 1f, 0f), attackEnemySpeed * 5 * Time.deltaTime);
            _animator.SetTrigger("AttackEnemy");
        }
    }

    private void StartToReturnAfterAttackingEnemy()
    {
        StartCoroutine(ReturnToTargetPlayerPos());
    }

    public void SetAttractEnemiesPositionAndFacePosition(GameObject targetPos, GameObject facePos)
    {
        attractEnemyPosition = targetPos.transform.position;
        attractEnemyFacePosition = facePos.transform.position;
    }

    public void UnSetAttractEnemiesPosition()
    {
        attractEnemyPosition = Vector3.zero;
    }

    public bool HasAttractEnemiesPosition()
    {
        return attractEnemyPosition != Vector3.zero;
    }

    public void StartOfAttractingEnemies()
    {
        m_isAttractingEnemy = true;
        navMeshAgent.enabled = false;
        beforeAttractEnemiesPosition = gameObject.transform.position;
        ChangeAIState(AIState.AttractEnemy);
        //DisableCollisionSystem();
    }

    private void AttractEnemies()
    {
        transform.position = Vector3.MoveTowards(transform.position, attractEnemyPosition, moveSpeed * Time.deltaTime);
        rb.constraints = RigidbodyConstraints.FreezeRotation;
        _animator.SetFloat(_animIDSpeed, 1f);
        _animator.SetFloat(_animIDMotionSpeed, 1f);
        if (Vector3.Distance(transform.position, attractEnemyPosition) <= 0f)
        {
            //EnableCollisionSystem();
            _animator.SetFloat(_animIDSpeed, 0f);
            _animator.SetFloat(_animIDMotionSpeed, 0f);
            FaceTarget(attractEnemyFacePosition);
        }
        // navMeshAgent.SetDestination(attractEnemyPosition);
    }

    public void EndOfAttractingEnemies()
    {
        //DisableCollisionSystem();
        m_isAttractingEnemy = false;
        ChangeAIState(AIState.EndOfAttractingEnemy);
    }

    private void ReturningFromAttractingEnemies()
    {
        transform.position = Vector3.MoveTowards(transform.position, beforeAttractEnemiesPosition, moveSpeed * Time.deltaTime);
        _animator.SetFloat(_animIDSpeed, 1f);
        _animator.SetFloat(_animIDMotionSpeed, 1f);
        if (Vector3.Distance(transform.position, beforeAttractEnemiesPosition) <= 0f)
        {
            _animator.SetFloat(_animIDSpeed, 0f);
            _animator.SetFloat(_animIDMotionSpeed, 0f);
            EnableCollisionSystem();
            navMeshAgent.enabled = true;
            FaceTarget(targetPlayer.transform.position);
            ChangeAIState(AIState.Normal);
        }
    }

    public bool IsAttractingEnemies()
    {
        return m_isAttractingEnemy;
    }

    private void OnCollisionEnter(Collision collider)
    {
        if(collider.gameObject.tag.Equals("Enemy") && collider.gameObject.GetComponent<Enemy>().CanbeAttackedByBird())
        {
            Enemy enemy = collider.gameObject.GetComponent<Enemy>();
            enemy.AttackedByBird();
            _animator.SetTrigger("ReturnFromAttack");
            ChangeAIState(AIState.EndOfAttackingEnemy);
            targetEnemy = null;
        }
    }

    public void EnableCollisionSystem()
    {
        capsuleCollider.enabled = true;
    }

    public void DisableCollisionSystem()
    {
        capsuleCollider.enabled = false;
    }
}
