using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDrop : MonoBehaviour
{
    public ParticleSystem waterDropEffect;
    public float destroyWaterDropEffectSeconds;
    public string waterDropName;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (!gameManager.CanUseBottle())
            {
                gameManager.ShowCannotCollectWaterDropHint();
            }
            else
            {
                if (waterDropEffect != null)
                {
                    ParticleSystem newWaterDropEffect = Instantiate(waterDropEffect);
                    newWaterDropEffect.transform.position = gameObject.transform.position;
                    newWaterDropEffect.transform.rotation = Quaternion.identity;
                    newWaterDropEffect.Play();
                    Destroy(newWaterDropEffect, destroyWaterDropEffectSeconds);
                }
                switch (gameObject.name)
                {
                    case "WaterDrop1":
                        GameObject earthquakeItem = GameObject.Find("EarthquakeEffectItem");
                        if (earthquakeItem != null)
                        {
                            earthquakeItem.GetComponent<FreeFallTrap>().ActivateFreeFallTrap();
                        }
                        Destroy(gameObject);
                        gameManager.ActivateDestroyBigStone();
                        break;
                    case "WaterDrop2":
                        MainSceneManager mainSceneManager = FindObjectOfType<MainSceneManager>();
                        GameObject earthquakeItem2 = GameObject.Find("EarthquakeEffectItem");
                        if (earthquakeItem2 != null)
                        {
                            earthquakeItem2.GetComponent<FreeFallTrap>().ActivateFreeFallTrap();
                        }
                        mainSceneManager.EnableHint14Trigger();
                        FindObjectOfType<MainSceneManager>().ActiviateChandelierFreeFall();
                        gameObject.SetActive(false);
                        break;
                    case "WaterDrop3":
                        FindObjectOfType<MainSceneManager>().ActivateEnemiesStartChasingPlayerFlag();
                        Destroy(gameObject);
                        break;
                }
                gameManager.CollectedWaterDrop(waterDropName);
                gameManager.HideCannotCollectWaterDropHint();
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            gameManager.HideCannotCollectWaterDropHint();
        }
    }
}
