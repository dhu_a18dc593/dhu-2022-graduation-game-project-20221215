using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HintTrigger : MonoBehaviour
{
    public GameObject targetItem;
    public bool showOnceOnlyFlag, targetItemNullFlag;
    public Hints hintsData;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            MainSceneManager mainSceneManager = FindObjectOfType<MainSceneManager>();
            CaveManager caveManager = FindObjectOfType<CaveManager>();
            string sceneName = SceneManager.GetActiveScene().name;
            string[] hintsContent = hintsData.GetHintContents();

            if (showOnceOnlyFlag)
            {
                if (gameManager.IsHintShowed(gameObject.name))
                {
                    return;
                }
                else
                {
                    gameManager.SetHintFlagShowed(gameObject.name);
                    if (sceneName.Contains("Main"))
                    {
                        mainSceneManager.ShowDialog(hintsContent, true);
                    }
                    else
                    {
                        caveManager.ShowDialog(hintsContent, true);
                    }
                }
            }
            else if (targetItemNullFlag ||
                (targetItem.tag.Equals("Ladder") && !FindObjectOfType<GameManager>().CanUseLadder()) || 
                (targetItem != null && 
                    !(targetItem.tag.Equals("Ladder") && FindObjectOfType<GameManager>().CanUseLadder()) &&
                    !(targetItem.tag.Equals("OpenDoorArea") && FindObjectOfType<GameManager>().GetWaterDropCount() >= 3)))
            {
                if (sceneName.Equals("MainScene"))
                {
                    mainSceneManager.ShowDialog(hintsContent, false);
                }
                else
                {
                    caveManager.ShowDialog(hintsContent, false);
                }
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            FindObjectOfType<GameManager>().HideAnkaDialog();
            if (gameObject.name.Equals("Hint14Trigger"))
            {
                FindObjectOfType<MainSceneManager>().DisableHint14Trigger();
            }
        }
    }
}
