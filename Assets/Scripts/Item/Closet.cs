using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Closet : MonoBehaviour
{
    public BoxCollider ClosetBoundary;

    private void OnTriggerStay(Collider collider)
    {
        if(collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            ThirdPersonController player = collider.gameObject.GetComponent<ThirdPersonController>();
            if (player.IsCrouching())
            {
                ClosetBoundary.isTrigger = true;
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            ThirdPersonController player = collider.gameObject.GetComponent<ThirdPersonController>();
            if (player.IsCrouching())
            {
                ClosetBoundary.isTrigger = false;
            }
        }
    }
}
