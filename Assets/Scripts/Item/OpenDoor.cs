using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    [SerializeField] bool canOpenDoor;

    private bool isDoorOpen;

    private void Start()
    {
        Initialization();
    }

    private void Initialization()
    {
        canOpenDoor = false;
    }

    public void EnableOpenDoorFlag()
    {
        canOpenDoor = true;
    }

    public void DisableOpenDoorFlag()
    {
        canOpenDoor = false;
    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            if (canOpenDoor)
            {
                FindObjectOfType<ThirdPersonController>().EnableOpenDoor();
                FindObjectOfType<GameManager>().ShowOpenDoorHintText();
            }
            else
            {
                FindObjectOfType<ThirdPersonController>().DisableOpenDoor();
                FindObjectOfType<GameManager>().HideOpenDoorHintText();
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            FindObjectOfType<ThirdPersonController>().DisableOpenDoor();
            FindObjectOfType<GameManager>().HideOpenDoorHintText();
        }
    }

    public void ProcessOpenDoor()
    {
        isDoorOpen = true;
    }

    public bool IsDoorOpen()
    {
        return isDoorOpen;
    }
}
