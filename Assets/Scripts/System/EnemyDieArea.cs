using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDieArea : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            Debug.Log("Game Object Name: "+gameObject.name);
            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            enemy.Die();
        }
    }
}
