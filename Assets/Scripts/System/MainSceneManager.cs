using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using Cinemachine;
using StarterAssets;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class MainSceneManager : MonoBehaviour
{
    public static MainSceneManager Instance
    {
        get; private set;
    }

    public Enemy[] chasePlayerEnemies;
    public GameObject ElderBrotherCharacter, YoungerBrotherCharacter, Bird;
    public GameObject ElderBrotherCharacterPrefab, YoungerBrotherCharacterPrefab, BirdPrefab;
    public GameObject DestroyableStone, DestroyableStoneBoundary, Chandelier;
    public GameObject CountDownCanvas, GameOverCanvas, FoundByEnemyCanvas, AnkaDialogCanvas, PauseCanvas;
    public GameObject bottleItem, ladderItem, WaterDrop2, WaterDrop3;
    public GameObject ladder1_part, ladder1_separate, ladder1_combined;
    public GameObject ladder2_part, ladder2_separate, ladder2_combined;
    public GameObject GameOverCheckPointButton, GameOverRestartButton;
    public GameObject PauseCheckPointButton, PauseResumeButton;
    public GameObject Enemy1, Enemy3, Enemy11, Enemy12;
    public GameObject SoloActionArea1, SoloActionArea2, SoloActionRestrictedArea1, Closet, ClosetBoundary, NonStandUpArea2;
    public GameObject hint14Trigger;
    public Hints hint14_2FromAnka;
    public BoxCollider ClimbLadderArea6Trigger;
    public NavMeshAgent ElderBrotherNavMeshAgent, YoungerBrotherNavMeshAgent, BirdNavMeshAgent;
    public AnimalCharacterAI BirdAI;
    public ThirdPersonController ElderBrotherCharacterController, YoungerBrotherCharacterController;
    public PlayerInput ElderBrotherCharacterInput, YoungerBrotherCharacterInput;
    public FollowCharacterAI ElderBrotherCharacterFollowCharacterAI, YoungerBrotherCharacterFollowCharacterAI;
    public Transform ElderBrotherCameraRoot, YoungerBrotherCameraRoot;
    public Transform ElderBrotherCameraRootBackMountain, YoungerBrotherCameraRootBackMountain;
    public Transform TestCheckPointPos;
    public CinemachineVirtualCamera PlayerFollowCamera;
    public float ChangeCharacterMoveSpeed = 2f, waitForRestartAtSpawnPointSeconds = 2f, fadeOutFadeInSeconds = 1f;
    public float respawnYoungerBrotherOffsetX = 2f, respawnBirdOffsetX = 2f;
    public float normalCameraDistance = 1.1f, chasePlayerCameraDistance = 10f, chandelierCameraDistance = 3.5f, zoomSpeed = 5f;
    public float zoomOutDelaySeconds = 2f, zoomInDelaySeconds = 1f;
    public float GameOverRestartButtonPosX, GameOverRestartButtonPosY;
    public float GameOverCheckPointButtonPosX, GameOverCheckPointButtonPosY;
    public float PauseResumeButtonPosX, PauseResumeButtonPosY;
    public float PauseCheckPointButtonPosX, PauseCheckPointButtonPosY;
    public Text CountDownTimeText, RemainingSecondsText, AnkaDialogText;
    public Image fadeInFadeOutImage, AnkaDialogPanel;
    public CanvasGroup ClimbFadeScreenCanvas;
    // 2022-06-20 by 楊
    // 3fではなく3.9fの理由は、"3"を画面上に1秒くらい表示したいからです。
    public float StartCountDownTime = 3.9f;
    public float HideCountDownTime = 1f;
    public float ShowGameOverTextTime = 2f;
    public float CountDownToGameOverTime = 3f;

    private Transform _currentElderBrotherPosition, _currentYoungerBrotherPosition, climbTriggerTransform;
    private MainSceneGameState CurrentGameState;
    private Color fadeOutColorAlpha;
    private GameObject _elderBrotherClimbTargetPosition, _youngerBrotherClimbTargetPosition, _birdClimbTargetPosition;
    private GameObject targetLadder;
    private CinemachineComponentBase cinemachineComponentBase;
    private AIJumpPoint targetRestoreAIJumpWaypoint;
    private float CountDownTimer, CountDownToGameOverTimer, FadeOutStartTime;
    private float targetRotationY;
    private float restartAtCheckPointButtonY, restartLevelButtonY;
    private int hintsFromAnkaIndex;
    private string[] hintsFromAnka;
    private string climbingControlCharacter;
    [SerializeField] private bool chasePlayerFlag;
    private static bool rotateCameraFlag;
    private static float rotateCameraAngle;

    public enum ControlCharacter
    {
        ElderBrother,
        YoungerBrother
    }

    enum MainSceneGameState
    {
        CountDown,
        GameStart,
        ShowHints,
        Respawn,
        FoundByEnemy,
        Pause,
        GameOver,
        StartClimbing,
        EndClimbing,
        EndChasingPlayer,
        ActiviateChandelierFreeFall,
    }

    private void Start()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        Initialization();
    }

    private void Update()
    {
        switch (CurrentGameState)
        {
            case MainSceneGameState.CountDown:
                Time.timeScale = 1f;
                DisableElderBrotherControl();
                CountDownTimeText.enabled = true;
                CountDownTimer -= Time.deltaTime;
                CountDown(CountDownTimer);
                break;
            case MainSceneGameState.GameStart:
                HideCountDownText();
                HideFoundByEnemyCanvas();
                HideGameOverCanvas();
                HidePauseMenu();
                if (chasePlayerFlag)
                {
                    EnemiesStartChasingPlayer();
                }
                else
                {
                    ArrivedAtChasingEndPoint();
                }
                UpdateWaterDropCount();
                break;
            case MainSceneGameState.ShowHints:
                ProcessToShowDialog();
                break;
            case MainSceneGameState.Respawn:
                StartCoroutine(RespawnEffectHandling());
                break;
            case MainSceneGameState.FoundByEnemy:
                CountDownToGameOver();
                break;
            case MainSceneGameState.Pause:
                ShowPauseMenu();
                break;
            case MainSceneGameState.GameOver:
                StartCoroutine(ShowGameOverText());
                break;
            case MainSceneGameState.StartClimbing:
                StartCoroutine(FadeInClimbFadeScreenCanvasGroup(ClimbFadeScreenCanvas, ClimbFadeScreenCanvas.alpha, 1));
                break;
            case MainSceneGameState.EndClimbing:
                StartCoroutine(FadeOutClimbFadeScreenCanvasGroup(ClimbFadeScreenCanvas, ClimbFadeScreenCanvas.alpha, 0));
                break;
            case MainSceneGameState.ActiviateChandelierFreeFall:
                StartCoroutine(ProcessChandelierFreeFall());
                break;
        }
    }

    private void Initialization()
    {
        InputSystem.DisableDevice(Mouse.current);
        InputSystem.EnableDevice(Keyboard.current);
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameManager.GetAnkaDialog();

        if (gameManager.IsExitFromCave())
        {
            GetOutFromCave(gameManager);

        }
        else
        {
            _currentElderBrotherPosition = ElderBrotherCharacter.transform;
            _currentYoungerBrotherPosition = YoungerBrotherCharacter.transform;

            if (gameManager.IsOnlyYoungerBrotherAction())
            {
                ElderBrotherCharacterController.enabled = false;
                ElderBrotherCharacterInput.enabled = false;
                ElderBrotherNavMeshAgent.enabled = true;
                YoungerBrotherCharacterInput.enabled = false;
                YoungerBrotherNavMeshAgent.enabled = false;
                YoungerBrotherCharacterController.enabled = true;
                YoungerBrotherCharacterFollowCharacterAI.enabled = false;
                YoungerBrotherCharacterController.enabled = true;
                YoungerBrotherCharacterInput.enabled = true;
                YoungerBrotherCharacterController.SoloAction();
                YoungerBrotherCharacter.GetComponent<StarterAssetsInputs>().InitSoloActionToTrue();
                BirdAI.SetTargetPlayer("ElderBrother");
                BirdAI.enabled = false;
                SetCameraToFollowControlingPlayer("YoungerBrother");
            }
            else
            {
                if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
                {
                    if (gameManager.IsRestartAtCheckPointFromCave() && !gameManager.GetLastCheckPointName().Equals(""))
                    {
                        YoungerBrotherCharacterFollowCharacterAI.enabled = false;
                        YoungerBrotherNavMeshAgent.enabled = false;
                        BirdAI.enabled = false;
                        BirdNavMeshAgent.enabled = false;
                        string checkPoint = gameManager.GetLastCheckPointName();
                        ElderBrotherCharacter.transform.position = GameObject.Find(checkPoint).transform.position;
                        YoungerBrotherCharacter.transform.position = ElderBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f);
                        Bird.transform.position = YoungerBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f);
                        BirdAI.enabled = true;
                        BirdNavMeshAgent.enabled = true;
                        gameManager.UnsetRestartAtCheckPointFromCave();
                        gameManager.SetGameState(GameManager.GameState.GameStart);
                    }
                    ElderBrotherCharacterFollowCharacterAI.enabled = false;
                    YoungerBrotherCharacterFollowCharacterAI.enabled = true;
                    ElderBrotherNavMeshAgent.enabled = false;
                    YoungerBrotherNavMeshAgent.enabled = true;
                    ElderBrotherCharacterController.enabled = true;
                    YoungerBrotherCharacterController.enabled = false;
                    ElderBrotherCharacterInput.enabled = true;
                    YoungerBrotherCharacterInput.enabled = false;
                    SetCameraToFollowControlingPlayer("ElderBrother");
                    BirdAI.SetTargetPlayer("YoungerBrother");
                }
                else
                {
                    if (gameManager.IsRestartAtCheckPointFromCave() && !gameManager.GetLastCheckPointName().Equals(""))
                    {
                        ElderBrotherCharacterFollowCharacterAI.enabled = false;
                        ElderBrotherNavMeshAgent.enabled = false;
                        BirdAI.enabled = false;
                        BirdNavMeshAgent.enabled = false;
                        string checkPoint = gameManager.GetLastCheckPointName();
                        YoungerBrotherCharacter.transform.position = GameObject.Find(checkPoint).transform.position;
                        ElderBrotherCharacter.transform.position = YoungerBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f);
                        Bird.transform.position = YoungerBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f);
                        BirdAI.enabled = true;
                        BirdNavMeshAgent.enabled = true;
                        gameManager.UnsetRestartAtCheckPointFromCave();
                        gameManager.SetGameState(GameManager.GameState.GameStart);
                    }
                    ElderBrotherCharacterFollowCharacterAI.enabled = true;
                    YoungerBrotherCharacterFollowCharacterAI.enabled = false;
                    ElderBrotherNavMeshAgent.enabled = true;
                    YoungerBrotherNavMeshAgent.enabled = false;
                    ElderBrotherCharacterController.enabled = false;
                    YoungerBrotherCharacterController.enabled = true;
                    ElderBrotherCharacterInput.enabled = false;
                    YoungerBrotherCharacterInput.enabled = true;
                    SetCameraToFollowControlingPlayer("YoungerBrother");
                    BirdAI.SetTargetPlayer("ElderBrother");
                }
            }
        }

        if (FindObjectOfType<GameManager>().GetGameState() == GameManager.GameState.CountDown)
        {
            CurrentGameState = MainSceneGameState.CountDown;
        }
        else
        {
            CurrentGameState = MainSceneGameState.GameStart;
        }
        CountDownCanvas.SetActive(true);
        GameOverCanvas.SetActive(false);
        FoundByEnemyCanvas.SetActive(false);
        AnkaDialogCanvas.SetActive(false);
        PauseCanvas.SetActive(false);
        hintsFromAnkaIndex = 0;
        CountDownTimer = StartCountDownTime;
        CountDownToGameOverTimer = CountDownToGameOverTime;
        restartAtCheckPointButtonY = GameOverCheckPointButton.transform.position.y;
        restartLevelButtonY = GameOverRestartButton.transform.position.y;
        rotateCameraFlag = false;
        rotateCameraAngle = 0f;
        chasePlayerFlag = false;
        cinemachineComponentBase = PlayerFollowCamera.GetCinemachineComponent(CinemachineCore.Stage.Body);
        targetRestoreAIJumpWaypoint = null;
        // ヒント14トリガーは、2つ目水玉を取ったら有効化します
        hint14Trigger.SetActive(false);
        if (gameManager.IsBigStoneDestroyed())
        {
            DestroyableStone.SetActive(false);
            DestroyableStoneBoundary.SetActive(false);
        }
        if (gameManager.IsWaterDrop2Collected())
        {
            WaterDrop2.SetActive(false);
        }
        if (gameManager.IsWaterDrop3Collected())
        {
            WaterDrop3.SetActive(false);
        }
        if (gameManager.CanUseBottle())
        {
            foreach(GameObject b in GameObject.FindGameObjectsWithTag("Bottle"))
            {
                b.SetActive(false);
            }
        }
        if (gameManager.IsLadder1Repaired())
        {
            ladder1_part.SetActive(false);
            ladder1_separate.SetActive(false);
            ladder1_combined.SetActive(true);
        }
        if (gameManager.IsLadder2Repaired())
        {
            ladder2_part.SetActive(false);
            ladder2_separate.SetActive(false);
            ladder2_combined.SetActive(true);
        }
        if (gameManager.IsEnemy1Died())
        {
            Enemy1.SetActive(false);
        }
        if (gameManager.IsEnemy3Died())
        {
            Enemy3.SetActive(false);
        }
        if (gameManager.IsEnemy11Died())
        {
            Enemy11.SetActive(false);
        }
        if (gameManager.IsEnemy12Died())
        {
            Enemy12.SetActive(false);
        }
        if (gameManager.IsClosetDestroyed())
        {
            SoloActionArea2.SetActive(false);
            SoloActionRestrictedArea1.SetActive(false);
            Closet.SetActive(false);
            ClosetBoundary.SetActive(false);
            NonStandUpArea2.SetActive(false);
        }
    }

    private void GetOutFromCave(GameManager gameManager)
    {
        if (gameManager.IsWaterDrop1Collected())
        {
            DisableElderBrotherControl();
            DisableYoungerBrotherControl();
            ElderBrotherNavMeshAgent.enabled = false;
            BirdNavMeshAgent.enabled = false;

            ElderBrotherCharacter.transform.position = gameManager.GetMainSceneElderBrotherPosition();
            ElderBrotherCharacter.transform.rotation = gameManager.GetMainSceneElderBrotherRotation();
            YoungerBrotherCharacter.transform.position = gameManager.GetMainSceneYoungerBrotherPosition() - new Vector3(0f, 0f, 1f);
            YoungerBrotherCharacter.transform.rotation = Quaternion.Euler(0f, -180f, 0f);
            Bird.transform.position = gameManager.GetMainSceneBirdPosition();
            Bird.transform.rotation = gameManager.GetMainSceneBirdRotation();

            YoungerBrotherCharacterFollowCharacterAI.enabled = false;
            YoungerBrotherNavMeshAgent.enabled = false;
            YoungerBrotherCharacterController.EnablePlayerAnimatorController();
            SetCameraToFollowControlingPlayer("YoungerBrother");

            ElderBrotherCharacterFollowCharacterAI.enabled = true;
            ElderBrotherCharacterFollowCharacterAI.EnableAIAnimatorController();
            ElderBrotherNavMeshAgent.enabled = true;

            BirdAI.enabled = true;
            BirdAI.SetTargetPlayer("ElderBrother");
            BirdNavMeshAgent.enabled = true;
            
            gameManager.SetOnlyYoungerBrotherAction(false);

            SoloActionArea1.SetActive(false);

            EnableYoungerBrotherControl();
        }
        else
        {
            if (FindObjectOfType<GameManager>().GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                DisableYoungerBrotherControl();
                YoungerBrotherNavMeshAgent.enabled = false;
                EnableElderBrotherControl();
            }
            else
            {
                YoungerBrotherCharacterFollowCharacterAI.enabled = false;
                YoungerBrotherNavMeshAgent.enabled = false;
                DisableElderBrotherControl();
                ElderBrotherNavMeshAgent.enabled = false;
                EnableYoungerBrotherControl();
                YoungerBrotherCharacterController.EnablePlayerAnimatorController();
                SetCameraToFollowControlingPlayer("YoungerBrother");
                BirdAI.enabled = true;
                BirdAI.SetTargetPlayer("ElderBrother");
                if (gameManager.IsOnlyYoungerBrotherAction())
                {
                    YoungerBrotherCharacterController.SetToSoloAction();
                    YoungerBrotherCharacterController.GetComponent<StarterAssetsInputs>().InitSoloActionToTrue();
                }
            }
            BirdNavMeshAgent.enabled = false;

            ElderBrotherCharacter.transform.position = gameManager.GetMainSceneElderBrotherPosition();
            ElderBrotherCharacter.transform.rotation = gameManager.GetMainSceneElderBrotherRotation();
            YoungerBrotherCharacter.transform.position = gameManager.GetMainSceneYoungerBrotherPosition() - new Vector3(0f, 0f, 1f);
            YoungerBrotherCharacter.transform.rotation = Quaternion.Euler(0f, -180f, 0f);
            Bird.transform.position = gameManager.GetMainSceneBirdPosition();
            Bird.transform.rotation = gameManager.GetMainSceneBirdRotation();

            if (GameObject.Find("EnterCaveArea1") != null)
            {
                BoxCollider enterCaveArea1 = GameObject.Find("EnterCaveArea1").GetComponent<BoxCollider>();
                enterCaveArea1.enabled = true;
                enterCaveArea1.isTrigger = true;
            }
        }
    }

    private void DisableElderBrotherControl()
    {

        ElderBrotherCharacterController.enabled = false;
        ElderBrotherCharacterInput.enabled = false;
        ElderBrotherCharacterController.StopAnimation();
    }
    private void EnableElderBrotherControl()
    {

        ElderBrotherCharacterController.enabled = true;
        ElderBrotherCharacterController.EnableControl();
        ElderBrotherCharacterInput.enabled = true;
    }

    private void DisableYoungerBrotherControl()
    {

        YoungerBrotherCharacterController.enabled = false;
        YoungerBrotherCharacterInput.enabled = false;
        YoungerBrotherCharacterController.StopAnimation();
    }
    private void EnableYoungerBrotherControl()
    {

        YoungerBrotherCharacterController.enabled = true;
        YoungerBrotherCharacterController.EnableControl();
        YoungerBrotherCharacterInput.enabled = true;
    }

    private void UpdateWaterDropCount()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameManager.ShowWaterDropImage();
    }

    private void ProcessToShowDialog()
    {
        if (Keyboard.current.enterKey.wasPressedThisFrame)
        {
            if (hintsFromAnkaIndex + 1 >= hintsFromAnka.Length)
            {
                GameManager gameManager = FindObjectOfType<GameManager>();
                gameManager.HideAnkaDialog();
                if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
                {
                    ElderBrotherCharacterInput.enabled = true;
                }
                else
                {
                    YoungerBrotherCharacterInput.enabled = true;
                }
                ChangeMainSceneState(MainSceneGameState.GameStart);
            }
            else
            {
                hintsFromAnkaIndex++;
                AnkaDialogText.text = hintsFromAnka[hintsFromAnkaIndex].Replace("|", Environment.NewLine);
            }
        }
    }

    public void ShowDialog(string[] hintsContent, bool showOnceOnlyFlag = false)
    {
        hintsFromAnka = hintsContent;
        if (showOnceOnlyFlag)
        {
            hintsFromAnkaIndex = 0;
            AnkaDialogText.text = hintsContent[hintsFromAnkaIndex].Replace("|", Environment.NewLine);
            AnkaDialogCanvas.SetActive(true);
            AnkaDialogPanel.color = new Color(AnkaDialogPanel.color.r, AnkaDialogPanel.color.b, AnkaDialogPanel.color.b, 0.25f);
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                ElderBrotherCharacterInput.enabled = false;
            }
            else
            {
                YoungerBrotherCharacterInput.enabled = false;
            }
            ChangeMainSceneState(MainSceneGameState.ShowHints);
        }
        else
        {
            hintsFromAnkaIndex = 0;
            AnkaDialogText.text = hintsContent[hintsFromAnkaIndex].Replace("|", Environment.NewLine);
            AnkaDialogCanvas.SetActive(true);
            AnkaDialogPanel.color = new Color(AnkaDialogPanel.color.r, AnkaDialogPanel.color.b, AnkaDialogPanel.color.b, 0f);
        }
    }

    private void ChangeMainSceneState(MainSceneGameState targetGameState)
    {
        CurrentGameState = targetGameState;
    }

    private void CountDown(float countDownTimer)
    {
        if (CountDownTimeText == null)
        {
            Instance.ChangeMainSceneState(MainSceneGameState.GameStart);
            return;
        }

        ShowCountTimeText(countDownTimer);
        if (countDownTimer <= 0.0f)
        {
            EnableElderBrotherControl();
            Invoke("HideCountDownText", HideCountDownTime);
            YoungerBrotherNavMeshAgent.enabled = true;
            Instance.ChangeMainSceneState(MainSceneGameState.GameStart);
            FindObjectOfType<GameManager>().SetGameState(GameManager.GameState.GameStart);
        }
    }

    private void ShowCountTimeText(float countDownTimer)
    {
        int remainingSeconds = Mathf.FloorToInt(countDownTimer);
        string startGameLabelText;
        startGameLabelText = "Start!";
        CountDownTimeText.text = (remainingSeconds > 0) ? remainingSeconds.ToString() : startGameLabelText;
    }

    private void HideCountDownText()
    {
        CountDownTimeText.enabled = false;
        CountDownCanvas.SetActive(false);
    }
    private void HideGameOverCanvas()
    {
        GameOverCanvas.SetActive(false);
    }

    public void ChangeControlPlayer(ControlCharacter player)
    {
        _currentElderBrotherPosition = ElderBrotherCharacter.transform;
        _currentYoungerBrotherPosition = YoungerBrotherCharacter.transform;
        switch (player)
        {
            case ControlCharacter.ElderBrother:
                YoungerBrotherCharacterController.StopAnimation();
                YoungerBrotherCharacterController.enabled = false;
                YoungerBrotherCharacterInput.enabled = false;
                SetCameraToFollowControlingPlayer("ElderBrother");
                ElderBrotherCharacterController.enabled = true;
                ElderBrotherCharacterController.EnablePlayerAnimatorController();
                ElderBrotherCharacterInput.enabled = true;
                ElderBrotherCharacterFollowCharacterAI.enabled = false;
                ElderBrotherNavMeshAgent.enabled = false;
                StartCoroutine(ChangeCharacterPosition("ElderBrother"));
                break;
            case ControlCharacter.YoungerBrother:
                ElderBrotherCharacterController.StopAnimation();
                ElderBrotherCharacterController.enabled = false;
                ElderBrotherCharacterInput.enabled = false;
                SetCameraToFollowControlingPlayer("YoungerBrother");
                YoungerBrotherCharacterController.enabled = true;
                YoungerBrotherCharacterController.EnablePlayerAnimatorController();
                YoungerBrotherCharacterInput.enabled = true;
                YoungerBrotherCharacterFollowCharacterAI.enabled = false;
                YoungerBrotherNavMeshAgent.enabled = false;
                StartCoroutine(ChangeCharacterPosition("YoungerBrother"));
                break;
        }

    }

    private IEnumerator ChangeCharacterPosition(string targetControlPlayer)
    {
        string targetAIPlayer = "";
        switch (targetControlPlayer)
        {
            case "ElderBrother":
                YoungerBrotherCharacterFollowCharacterAI.enabled = true;
                YoungerBrotherNavMeshAgent.enabled = true;
                YoungerBrotherCharacterFollowCharacterAI.MoveBehindControlPlayer();
                YoungerBrotherCharacterFollowCharacterAI.EnableAIAnimatorController();
                targetAIPlayer = "YoungerBrother";
                break;
            case "YoungerBrother":
                ElderBrotherCharacterFollowCharacterAI.enabled = true;
                ElderBrotherNavMeshAgent.enabled = true;
                ElderBrotherCharacterFollowCharacterAI.MoveBehindControlPlayer();
                ElderBrotherCharacterFollowCharacterAI.EnableAIAnimatorController();
                targetAIPlayer = "ElderBrother";
                break;
        }
        yield return new WaitForSeconds(0.5f);
        StartCoroutine(ChangeAnimalPosition(targetAIPlayer));
    }
    private IEnumerator ChangeAnimalPosition(string targetAIPlayer)
    {
        switch (targetAIPlayer)
        {
            case "ElderBrother":
                BirdAI.SetTargetPlayer("ElderBrother");
                BirdNavMeshAgent.SetDestination(ElderBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f));
                break;
            case "YoungerBrother":
                BirdAI.SetTargetPlayer("YoungerBrother");
                BirdNavMeshAgent.SetDestination(YoungerBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f));
                break;
        }
        yield return null;
    }


    public void SetClimbTargetPosition(GameObject elderBrotherClimbTargetPosition, GameObject youngerBrotherClimbTargetPosition, 
        GameObject birdClimbTargetPosition, float targetRotationY, Transform climbTriggerTransform, bool rotateCamFlg, float targetRotationAngle)
    {
        _elderBrotherClimbTargetPosition = elderBrotherClimbTargetPosition;
        _youngerBrotherClimbTargetPosition = youngerBrotherClimbTargetPosition;
        _birdClimbTargetPosition = birdClimbTargetPosition;
        this.targetRotationY = targetRotationY;
        this.climbTriggerTransform = climbTriggerTransform;
        rotateCameraFlag = rotateCamFlg;
        rotateCameraAngle = targetRotationAngle;
    }

    public void CharacterPerformClimbing(string controlCharacter)
    {
        climbingControlCharacter = controlCharacter;
        InputSystem.DisableDevice(Keyboard.current);
        FindObjectOfType<GameManager>().HideClimbHint();
        Instance.ChangeMainSceneState(MainSceneGameState.StartClimbing);
    }

    private IEnumerator UpdateCharacterPositionFromClimbing()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        if (rotateCameraFlag)
        {
            switch (climbingControlCharacter)
            {
                case "ElderBrother":
                    ElderBrotherCharacterController.OverrideCameraAngle(rotateCameraAngle);
                    if (ElderBrotherCharacterController.GetClimbTriggerName().Equals("EnterToBackMountainArea"))
                    {
                        gameManager.GoToBackMountain();
                    }
                    else
                    {
                        gameManager.ExitFromBackMountain();
                    }
                    break;
                case "YoungerBrother":
                    YoungerBrotherCharacterController.OverrideCameraAngle(rotateCameraAngle);
                    if (YoungerBrotherCharacterController.GetClimbTriggerName().Equals("EnterToBackMountainArea"))
                    {
                        gameManager.GoToBackMountain();
                    }
                    else
                    {
                        gameManager.ExitFromBackMountain();
                    }
                    break;
            }
        }
        if (gameManager.IsOnlyYoungerBrotherAction())
        {
            YoungerBrotherCharacter.transform.position = _youngerBrotherClimbTargetPosition.transform.position;
            YoungerBrotherCharacter.transform.localEulerAngles = new Vector3(0f, targetRotationY, 0f);
            yield return new WaitForSeconds(0.5f);
        }
        else
        {
            ElderBrotherNavMeshAgent.enabled = false;
            YoungerBrotherNavMeshAgent.enabled = false;
            BirdNavMeshAgent.enabled = false;
            if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                ElderBrotherCharacter.transform.position = _elderBrotherClimbTargetPosition.transform.position;
                ElderBrotherCharacter.transform.localEulerAngles = new Vector3(0f, targetRotationY, 0f);
                YoungerBrotherCharacter.transform.position = _youngerBrotherClimbTargetPosition.transform.position;
                YoungerBrotherCharacter.transform.localEulerAngles = new Vector3(0f, targetRotationY, 0f);
            }
            else
            {
                YoungerBrotherCharacter.transform.position = _elderBrotherClimbTargetPosition.transform.position;
                YoungerBrotherCharacter.transform.localEulerAngles = new Vector3(0f, targetRotationY, 0f);
                ElderBrotherCharacter.transform.position = _youngerBrotherClimbTargetPosition.transform.position;
                ElderBrotherCharacter.transform.localEulerAngles = new Vector3(0f, targetRotationY, 0f);
            }
            Bird.transform.position = _birdClimbTargetPosition.transform.position;
            Bird.transform.localEulerAngles = new Vector3(0f, targetRotationY, 0f);
            //PlayerFollowCamera.enabled = false;
            yield return new WaitForSeconds(0.5f);
            //PlayerFollowCamera.enabled = true;
            switch (climbingControlCharacter)
            {
                case "ElderBrother":
                    YoungerBrotherNavMeshAgent.enabled = true;
                    break;
                case "YoungerBrother":
                    ElderBrotherNavMeshAgent.enabled = true;
                    break;
            }
            BirdNavMeshAgent.enabled = true;
        }
    }

    public void EnemiesStartChasingPlayer()
    {
        foreach(Enemy e in chasePlayerEnemies)
        {
            e.ChasePlayer();
        }
        if(cinemachineComponentBase is Cinemachine3rdPersonFollow)
        {
            (cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance += zoomSpeed * Time.deltaTime;
            if((cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance >= chasePlayerCameraDistance)
            {
                (cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance = chasePlayerCameraDistance;
            }
        }
    }

    public void ArrivedAtChasingEndPoint()
    {
        if (cinemachineComponentBase is Cinemachine3rdPersonFollow)
        {
            (cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance -= zoomSpeed * Time.deltaTime;
            if ((cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance <= normalCameraDistance)
            {
                (cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance = normalCameraDistance;
            }
        }
    }

    public void ActivateEnemiesStartChasingPlayerFlag()
    {
        chasePlayerFlag = true;
    }

    public void EnemiesEndOfChasingPlayer()
    {
        chasePlayerFlag = false;
    }

    public void DestroyStone()
    {
        DestroyableStone.SetActive(false);
    }

    public void ActiviateChandelierFreeFall()
    {
        ChangeMainSceneState(MainSceneGameState.ActiviateChandelierFreeFall);
    }

    private IEnumerator ProcessChandelierFreeFall()
    {
        Debug.Log("ActiviateChandelierFreeFall");
        CinemachineVirtualCamera PlayerFollowCamera = FindObjectOfType<CinemachineVirtualCamera>();
        CinemachineComponentBase cinemachineComponentBase = PlayerFollowCamera.GetCinemachineComponent(CinemachineCore.Stage.Body);

        if (cinemachineComponentBase is Cinemachine3rdPersonFollow)
        {
            (cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance += zoomSpeed * Time.deltaTime;
            if ((cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance >= chandelierCameraDistance)
            {
                (cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance = chandelierCameraDistance;
                if (Chandelier != null)
                {
                    Chandelier.GetComponent<FreeFallTrap>().ActivateFreeFallTrap();
                }
            }
        }

        yield return new WaitForSeconds(zoomOutDelaySeconds);

        if (cinemachineComponentBase is Cinemachine3rdPersonFollow)
        {
            (cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance -= zoomSpeed * Time.deltaTime;
            if ((cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance <= normalCameraDistance)
            {
                (cinemachineComponentBase as Cinemachine3rdPersonFollow).CameraDistance = normalCameraDistance;
            }
        }

        ChangeMainSceneState(MainSceneGameState.GameStart);
    }

    private IEnumerator FadeInClimbFadeScreenCanvasGroup(CanvasGroup canvasGroup, float start, float end, float lerpTime = 0.5f)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted;
        float percentageComplete;
        float test2 = 0f;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);
            float currentValue2 = test2 += Time.deltaTime;

            canvasGroup.alpha += Time.deltaTime;
            
            if(canvasGroup.alpha >= 1f)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1f);

        StartCoroutine(UpdateCharacterPositionFromClimbing());
        Instance.ChangeMainSceneState(MainSceneGameState.EndClimbing);
    }

    private IEnumerator FadeOutClimbFadeScreenCanvasGroup(CanvasGroup canvasGroup, float start, float end, float lerpTime = 1f)
    {
        float _timeStartedLerping = Time.time;
        float timeSinceStarted;
        float percentageComplete;

        while (true)
        {
            timeSinceStarted = Time.time - _timeStartedLerping;
            percentageComplete = timeSinceStarted / lerpTime;

            float currentValue = Mathf.Lerp(start, end, percentageComplete);

            canvasGroup.alpha -= Time.deltaTime;

            if (canvasGroup.alpha <= 0f)
            {
                break;
            }

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.5f);
        InputSystem.EnableDevice(Keyboard.current);
        Instance.ChangeMainSceneState(MainSceneGameState.GameStart);
    }

    public void SetCameraToFollowControlingPlayer(string targetCameraRoot)
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        Debug.Log("gameManager.IsInBackMountain(): "+ gameManager.IsInBackMountain());
        if (gameManager.IsInBackMountain())
        {
            ElderBrotherCameraRoot.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
            YoungerBrotherCameraRoot.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        }
        else
        {
            ElderBrotherCameraRoot.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
            YoungerBrotherCameraRoot.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        }
        switch (targetCameraRoot)
        {
            case "ElderBrother":
                PlayerFollowCamera.Follow = ElderBrotherCameraRoot;
                break;
            case "YoungerBrother":
                PlayerFollowCamera.Follow = YoungerBrotherCameraRoot;
                break;
        }
    }

    public void GameOver()
    {
        InputSystem.EnableDevice(Mouse.current);
        Instance.ChangeMainSceneState(MainSceneGameState.GameOver);
    }

    private IEnumerator ShowGameOverText()
    {
        yield return new WaitForSeconds(ShowGameOverTextTime);

        foreach (var e in FindObjectsOfType<Enemy>())
        {
            e.Stop();
            e.ChangeAnimationState("Idle");
        }

        GameOverCanvas.SetActive(true);

        if (FindObjectOfType<GameManager>().GetLastCheckPointName().Equals(""))
        {
            GameOverCheckPointButton.SetActive(false);
            RectTransform rectTransform = GameOverRestartButton.GetComponent<RectTransform>();
            Vector2 anchoredPos = rectTransform.anchoredPosition;
            anchoredPos.x = GameOverCheckPointButtonPosX;
            anchoredPos.y = GameOverCheckPointButtonPosY;
            rectTransform.anchoredPosition = anchoredPos;
        }
        else
        {
            GameOverCheckPointButton.SetActive(true);
            RectTransform rectTransform = GameOverRestartButton.GetComponent<RectTransform>();
            Vector2 anchoredPos = rectTransform.anchoredPosition;
            anchoredPos.x = GameOverRestartButtonPosX;
            anchoredPos.y = GameOverRestartButtonPosY;
            rectTransform.anchoredPosition = anchoredPos;
        }

        InputSystem.EnableDevice(Mouse.current);
    }

    public void RestartAtSpawnPoint(string playerName, Vector3 respawnPointPosition)
    {
        StartCoroutine(RestartAtSpawnPointProcess(playerName, respawnPointPosition));
    }

    private IEnumerator RestartAtSpawnPointProcess(string playerName, Vector3 respawnPointPosition)
    {
        FadeOutStartTime = Time.time;
        PlayerFollowCamera.enabled = false;
        BirdNavMeshAgent.enabled = false;
        yield return new WaitForSeconds(waitForRestartAtSpawnPointSeconds);

        if (playerName.Equals("ElderBrother"))
        {
            YoungerBrotherNavMeshAgent.enabled = false;
            ElderBrotherCharacter.transform.position = respawnPointPosition + new Vector3(0f, 0.5f, 0f);
            ElderBrotherCharacter.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            YoungerBrotherCharacter.transform.position = respawnPointPosition + new Vector3(0f, 0.1f, -1f);
            YoungerBrotherCharacter.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
        else
        {
            ElderBrotherNavMeshAgent.enabled = false;
            YoungerBrotherCharacter.transform.position = respawnPointPosition + new Vector3(0f, 0.5f, 0f);
            YoungerBrotherCharacter.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            ElderBrotherCharacter.transform.position = respawnPointPosition + new Vector3(0f, 0.1f, -1f);
            ElderBrotherCharacter.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }

        Bird.transform.position = respawnPointPosition + new Vector3(0f, 1f, -2f);
        Bird.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

        if (playerName.Equals("ElderBrother"))
        {
            YoungerBrotherNavMeshAgent.enabled = true;
        }
        else
        {
            ElderBrotherNavMeshAgent.enabled = true;
        }
        BirdNavMeshAgent.enabled = true;

        PlayerFollowCamera.enabled = true;
    }

    private IEnumerator RespawnEffectHandling()
    {
        yield return new WaitForSeconds(1f);

        fadeOutColorAlpha.a = (Time.time - FadeOutStartTime) / fadeOutFadeInSeconds;
        fadeInFadeOutImage.color = new Color(0, 0, 0, fadeOutColorAlpha.a);
        if(fadeOutColorAlpha.a <= 0f)
        {
            Instance.ChangeMainSceneState(MainSceneGameState.GameStart);
        }
    }

    private void ShowPauseMenu()
    {
        PauseCanvas.SetActive(true);
        GameManager gameManager = FindObjectOfType<GameManager>();
        if (gameManager.GetLastCheckPointName().Equals(""))
        {
            PauseCheckPointButton.SetActive(false);
            RectTransform rectTransform = PauseResumeButton.GetComponent<RectTransform>();
            Vector2 anchoredPos = rectTransform.anchoredPosition;
            anchoredPos.x = PauseCheckPointButtonPosX;
            anchoredPos.y = PauseCheckPointButtonPosY;
            rectTransform.anchoredPosition = anchoredPos;
        }
        else
        {
            PauseCheckPointButton.SetActive(true);
            RectTransform rectTransform = PauseResumeButton.GetComponent<RectTransform>();
            Vector2 anchoredPos = rectTransform.anchoredPosition;
            anchoredPos.x = PauseResumeButtonPosX;
            anchoredPos.y = PauseResumeButtonPosY;
            rectTransform.anchoredPosition = anchoredPos;
        }
    }

    private void HidePauseMenu()
    {
        PauseCanvas.SetActive(false);
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        Instance.ChangeMainSceneState(MainSceneGameState.Pause);
        GameManager gameManager = FindObjectOfType<GameManager>();
        if (gameManager.IsOnlyYoungerBrotherAction())
        {
            DisableYoungerBrotherControl();
        }
        else if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
        {
            DisableElderBrotherControl();
        }
        else
        {
            DisableYoungerBrotherControl();
        }
        InputSystem.EnableDevice(Mouse.current);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
        Instance.ChangeMainSceneState(MainSceneGameState.GameStart);
        GameManager gameManager = FindObjectOfType<GameManager>();
        if (gameManager.IsOnlyYoungerBrotherAction())
        {
            EnableYoungerBrotherControl();
        }
        else if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
        {
            EnableElderBrotherControl();
        }
        else
        {
            EnableYoungerBrotherControl();
        }
        InputSystem.DisableDevice(Mouse.current);
    }

    public void RestartAtLastCheckPoint()
    {
        ElderBrotherNavMeshAgent.enabled = false;
        YoungerBrotherNavMeshAgent.enabled = false;
        BirdNavMeshAgent.enabled = false;

        GameManager gameManager = FindObjectOfType<GameManager>();

        if (gameManager.IsExitFromCave())
        {
            GetOutFromCave(gameManager);
        }
        else
        {
            string lastCheckPointName = FindObjectOfType<GameManager>().GetLastCheckPointName();
            Vector3 lastCheckPointPosition = GameObject.Find(lastCheckPointName).GetComponent<Transform>().position;

            if (gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                ElderBrotherCharacter.transform.position = lastCheckPointPosition + new Vector3(0f, 0.5f, 0f);
                ElderBrotherCharacter.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                ElderBrotherCharacter.transform.Find("ElderBrotherCameraRoot").transform.rotation = Quaternion.Euler(0f, -90f, 0f);
                YoungerBrotherCharacter.transform.position = lastCheckPointPosition + new Vector3(0f, 0.1f, -1f);
                YoungerBrotherCharacter.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                YoungerBrotherCharacter.transform.Find("YoungerBrotherCameraRoot").transform.rotation = Quaternion.Euler(0f, -90f, 0f);

                EnableElderBrotherControl();
                ElderBrotherCharacterFollowCharacterAI.enabled = false;
                ElderBrotherNavMeshAgent.enabled = false;
                YoungerBrotherCharacterController.enabled = false;
                YoungerBrotherCharacterFollowCharacterAI.enabled = true;
                YoungerBrotherNavMeshAgent.enabled = true;
            }
            else
            {
                YoungerBrotherCharacter.transform.position = lastCheckPointPosition + new Vector3(0f, 0.5f, 0f);
                YoungerBrotherCharacter.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                YoungerBrotherCharacter.transform.Find("YoungerBrotherCameraRoot").transform.rotation = Quaternion.Euler(0f, -90f, 0f);
                ElderBrotherCharacter.transform.position = lastCheckPointPosition + new Vector3(0f, 0.1f, -1f);
                ElderBrotherCharacter.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                ElderBrotherCharacter.transform.Find("ElderBrotherCameraRoot").transform.rotation = Quaternion.Euler(0f, -90f, 0f);

                EnableYoungerBrotherControl();
                YoungerBrotherCharacterFollowCharacterAI.enabled = false;
                YoungerBrotherNavMeshAgent.enabled = false;
                ElderBrotherCharacterController.enabled = false;
                ElderBrotherCharacterFollowCharacterAI.enabled = true;
                ElderBrotherNavMeshAgent.enabled = true;

            }
            Bird.transform.position = lastCheckPointPosition + new Vector3(0f, 1f, -2f);
            Bird.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

            BirdAI.enabled = true;
            BirdNavMeshAgent.enabled = true;
            if (FindObjectOfType<GameManager>().GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                BirdAI.SetTargetPlayer("YoungerBrother");
                BirdNavMeshAgent.SetDestination(YoungerBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f));
            }
            else
            {
                BirdAI.SetTargetPlayer("ElderBrother");
                BirdNavMeshAgent.SetDestination(ElderBrotherCharacter.transform.position - new Vector3(0f, 0f, 1f));
            }
            BirdAI.UnsetTargetEnemy();
        }

        foreach (var e in FindObjectsOfType<Enemy>())
        {
            e.RestorePreviousState();
        }
        InputSystem.DisableDevice(Mouse.current);
        Instance.ChangeMainSceneState(MainSceneGameState.GameStart);
    }

    public void RestartLevel()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameManager.SetGameState(GameManager.GameState.CountDown);
        gameManager.ChangeControlPlayerName(GameManager.ControlCharacter.ElderBrother);
        gameManager.ResetAllHintTriggers();
        gameManager.ResetAllEnemiesToAlive();
        CountDownTimer = StartCountDownTime;
        SceneManager.LoadScene("MainScene");
    }

    public void ToTitle()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameManager.SetGameState(GameManager.GameState.CountDown);
        gameManager.ChangeControlPlayerName(GameManager.ControlCharacter.ElderBrother);
        CountDownTimer = StartCountDownTime;
        SceneManager.LoadScene("Title");
    }

    private void HideFoundByEnemyCanvas()
    {
        CountDownToGameOverTimer = CountDownToGameOverTime;
        FoundByEnemyCanvas.SetActive(false);

    }

    public void FoundByEnemy()
    {
        CountDownToGameOverTimer = CountDownToGameOverTime;
        Instance.ChangeMainSceneState(MainSceneGameState.FoundByEnemy);
    }

    public void EscapeFromEnemy()
    {
        CountDownToGameOverTimer = CountDownToGameOverTime;
        Instance.ChangeMainSceneState(MainSceneGameState.GameStart);
    }

    private void CountDownToGameOver()
    {
        FoundByEnemyCanvas.SetActive(true);
        CountDownToGameOverTimer -= Time.deltaTime;
        if(CountDownToGameOverTimer > 0.0f)
        {
            RemainingSecondsText.text = Mathf.FloorToInt(CountDownToGameOverTimer).ToString();
        }
        if (CountDownToGameOverTimer <= 0.0f)
        {
            Instance.ChangeMainSceneState(MainSceneGameState.GameOver);
        }
    }

    public void EnterCave()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameManager.SaveMainSceneCharacterPositions(ElderBrotherCharacter.transform, YoungerBrotherCharacter.transform, Bird.transform);
        gameManager.EnterCave();
        Initiate.Fade("CaveScene", Color.black, 1.0f);
    }

    public void SetTargetRestoreAIJumpWaypoint(AIJumpPoint waypoint)
    {
        targetRestoreAIJumpWaypoint = waypoint;
    }

    public void RestoreAIJumpWaypointObject()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        if (!gameManager.IsOnlyYoungerBrotherAction() && targetRestoreAIJumpWaypoint != null)
        {
            targetRestoreAIJumpWaypoint.RestoreOriginalJumpTargetPos();
        }
    }

    public void EnableHint14Trigger()
    {
        hint14Trigger.SetActive(true);
    }

    public void DisableHint14Trigger()
    {
        hint14Trigger.SetActive(false);
    }

    public void EnableClimbLadderArea6Trigger()
    {
        ClimbLadderArea6Trigger.enabled = true;
    }

    public void DisableClimbLadderArea6Trigger()
    {
        ClimbLadderArea6Trigger.enabled = false;
    }

    public void ChangeHint14Content()
    {
        string hint = hint14_2FromAnka.GetHintContent(0);
        AnkaDialogText.text = hint.Replace("|", Environment.NewLine);
    }
}
