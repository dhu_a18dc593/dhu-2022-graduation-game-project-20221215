using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbLadderTrigger : MonoBehaviour
{
    public GameObject Ladder;
    public GameObject ElderBrotherClimbTargetPosition, YoungerBrotherClimbTargetPosition, BirdClimbTargetPosition;
    public float targetRotationY = 0f;
    public string hint;
    public bool canClimbFlag, rotateCameraFlag = false, goToBackMountainFlag = false;

    private void OnTriggerStay(Collider collider)
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameManager.SetTargetLadder(Ladder);
        if(canClimbFlag)
        {
            FindObjectOfType<MainSceneManager>().SetClimbTargetPosition(ElderBrotherClimbTargetPosition,
                YoungerBrotherClimbTargetPosition, BirdClimbTargetPosition, targetRotationY, gameObject.transform, rotateCameraFlag, targetRotationY);
            if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
            {
                ThirdPersonController player = collider.gameObject.GetComponent<ThirdPersonController>();
                player.EnableClimbing();
                player.SetClimbTrigger(GetComponent<BoxCollider>());
                FindObjectOfType<GameManager>().ShowClimbHint(hint);
            }
        }
        else
        {
            if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled &&
                FindObjectOfType<GameManager>().CanUseLadder())
            {
                ThirdPersonController player = collider.gameObject.GetComponent<ThirdPersonController>();
                player.EnableUseItemFlag();
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled)
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            collider.gameObject.GetComponent<ThirdPersonController>().DisableUseItemFlag();
            collider.gameObject.GetComponent<ThirdPersonController>().DisableClimbing();
            gameManager.HideClimbHint();
        }
    }

    public void EnableCanClimbFlag()
    {
        canClimbFlag = true;
    }
}
