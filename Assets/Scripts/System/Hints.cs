using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "HintsFromAnka")]
public class Hints : ScriptableObject
{
    public string[] hintContent;

    public string[] GetHintContents()
    {
        return hintContent;
    }

    public string GetHintContent(int index = 0)
    {
        return hintContent[index];
    }
}
