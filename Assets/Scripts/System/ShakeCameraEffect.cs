using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ShakeCameraEffect : MonoBehaviour
{
    public static ShakeCameraEffect Instance { get; set; }

    private CinemachineVirtualCamera cinemachineVirtualCamera;
    private float shakeTimer, shakeTimerTotal, shakeStartingIntensity;

    private void Awake()
    {
        Instance = this;
        cinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
    }


    public void ShakeCamera(float intensity, float time)
    {
        if (cinemachineVirtualCamera == null) { return; }

        CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin =
               cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = intensity;
        shakeStartingIntensity = intensity;
        shakeTimer = time;
        shakeTimerTotal = time;
    }

    private void Update()
    {
        if(shakeTimer > 0)
        {
            CinemachineBasicMultiChannelPerlin cinemachineBasicMultiChannelPerlin =
                cinemachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            shakeTimer -= Time.deltaTime;
            cinemachineBasicMultiChannelPerlin.m_FrequencyGain = shakeTimer;
            if (shakeTimer <= 0f)
            {
                cinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 
                    Mathf.Lerp(shakeStartingIntensity, 0f, (1f - shakeTimer / shakeTimerTotal));
            }
        }
    }
}
