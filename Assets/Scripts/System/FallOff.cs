using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FallOff : MonoBehaviour
{
    public Transform respawnPointPosition;

    private void OnTriggerEnter(Collider collider)
    {
        Debug.Log("respawnPointPosition.GetComponent<Transform>().transform.position: " + respawnPointPosition.GetComponent<Transform>().transform.position);
        Debug.Log("respawnPointPosition.transform.position: " + respawnPointPosition.transform.position);
        Debug.Log("respawnPointPosition.position: " + respawnPointPosition.position);

        if (collider.gameObject.tag.Equals("Player"))
        {
            FindObjectOfType<MainSceneManager>().RestartAtSpawnPoint(collider.gameObject.name, respawnPointPosition.GetComponent<Transform>().transform.position);
        }
        else if (collider.gameObject.tag.Equals("Enemy"))
        {
            Destroy(collider.gameObject, 1f);
        }
    }
}
