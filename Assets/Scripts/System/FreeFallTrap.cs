using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class FreeFallTrap : MonoBehaviour
{
    public bool shakeEffectFlag, disappearFlag;
    public float shakeCameraIntensity = 5f, shakeCameraTime = 1f, disppearTime = 0.5f;
    public GameObject targetItem;


    [SerializeField] bool m_TrapActivated;
    private Rigidbody rb;
    private BoxCollider boxCollider;

    // Start is called before the first frame update
    void Awake()
    {
        Initialization();
    }

    private void Initialization()
    {
        m_TrapActivated = false;
        rb = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_TrapActivated)
        {
            StartFreeFallTrap();
        }
    }

    public bool IsTrapActivated()
    {
        return m_TrapActivated;
    }

    public void ActivateFreeFallTrap()
    {
        m_TrapActivated = true;
    }

    private void StartFreeFallTrap()
    {
        rb.constraints = RigidbodyConstraints.FreezeRotation;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Enemy"))
        {
            collider.gameObject.GetComponent<Enemy>().Die();
            boxCollider.isTrigger = false;
        }
        else if (collider.gameObject.tag.Equals("Floor") || collider.gameObject.tag.Equals("Wall"))
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
        else if (collider.gameObject.tag.Equals("Closet"))
        {
            Destroy(targetItem);
            Destroy(collider.gameObject);
            if (GameObject.Find("NonStandUpArea2") != null)
            {
                GameObject.Find("NonStandUpArea2").SetActive(false);
            }
            if(GameObject.Find("SoloActionArea2") != null)
            {
                GameObject.Find("SoloActionArea2").GetComponent<SoloActionArea>().EnableEndOfSoloActionFlag();
            }
            FindObjectOfType<GameManager>().ActivateDestroyCloset();
            MainSceneManager mainSceneManager = FindObjectOfType<MainSceneManager>();
            mainSceneManager.ChangeHint14Content();
        }
        if (shakeEffectFlag)
        {
            ShakeCameraEffect.Instance.ShakeCamera(shakeCameraIntensity, shakeCameraTime);
            if (disappearFlag)
            {
                Destroy(gameObject, shakeCameraTime + disppearTime);
            }
        }
        else if (disappearFlag && m_TrapActivated)
        {
            Destroy(gameObject, disppearTime);
        }
    }

    private void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.tag.Equals("Floor") || collider.gameObject.tag.Equals("Wall"))
        {
            rb.constraints = RigidbodyConstraints.FreezeAll;
        }
        else if(collider.gameObject.tag.Equals("Closet"))
        {
            Destroy(targetItem);
            Destroy(collider.gameObject);
            if(GameObject.Find("NonStandUpArea2") != null)
            {
                GameObject.Find("NonStandUpArea2").SetActive(false);
            }
            if (GameObject.Find("SoloActionArea2") != null)
            {
                GameObject.Find("SoloActionArea2").GetComponent<SoloActionArea>().EnableEndOfSoloActionFlag();
            }
            FindObjectOfType<GameManager>().ActivateDestroyCloset();
            MainSceneManager mainSceneManager = FindObjectOfType<MainSceneManager>();
            mainSceneManager.ChangeHint14Content();
        }
        if (shakeEffectFlag)
        {
            ShakeCameraEffect.Instance.ShakeCamera(shakeCameraIntensity, shakeCameraTime);
            if (disappearFlag)
            {
                Destroy(gameObject, shakeCameraTime + disppearTime);
            }
        }
        else if (disappearFlag && m_TrapActivated)
        {
            Destroy(gameObject, disppearTime);
        }
    }
}
