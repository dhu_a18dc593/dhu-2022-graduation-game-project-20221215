using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropDownPickedItem : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Bird"))
        {
            FindObjectOfType<AnimalCharacterAI>().ItemPickedToDestintion();
        }
    }
}
