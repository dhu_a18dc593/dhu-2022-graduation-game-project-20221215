using Cinemachine;
using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneArea : MonoBehaviour
{
    public CinemachineVirtualCamera PlayerFollowCamera;
    public GameObject ElderBrotherCharacterCameraRoot, YoungerBroherCharacterCameraRoot;
    public bool changeCameraAngle = true;
    public Transform cameraAngle;

    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            ThirdPersonController player = collider.gameObject.GetComponent<ThirdPersonController>();
            if (gameObject.name.Equals("EnterCaveArea1"))
            {
                if (player.IsCrouching())
                {
                    player.EnableChangeSceneFlag();
                }
                else
                {
                    player.DisableChangeSceneFlag();
                }
            }
            else
            {
                player.EnableChangeSceneFlag();
            }
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (SceneManager.GetActiveScene().name.Equals("MainScene"))
            {
                if (gameObject.name.Equals("EnterCaveArea1"))
                {
                    if (player.IsCrouching())
                    {
                        gameManager.ShowEnterCaveHintText();
                    }
                    else
                    {
                        gameManager.HideEnterCaveHintText();
                    }
                }
                else
                {
                    gameManager.ShowEnterCaveHintText();
                }
                gameManager.SetEnterCaveAreaName(gameObject.name);
                if (gameObject.name.Equals("EnterCaveArea2"))
                {
                    gameManager.EnterFromBackMountain();
                }
            }
            else if (SceneManager.GetActiveScene().name.Equals("CaveScene"))
            {
                gameManager.ShowExitCaveHintText();
            }
            if(changeCameraAngle)
            {
                PlayerFollowCamera.Follow = cameraAngle;
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            ThirdPersonController player = collider.gameObject.GetComponent<ThirdPersonController>();
            player.DisableChangeSceneFlag();
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (SceneManager.GetActiveScene().name.Equals("MainScene"))
            {
                gameManager.HideEnterCaveHintText();
                if (gameObject.name.Equals("EnterCaveArea2"))
                {
                    gameManager.NotEnterFromBackMountain();
                }
            }
            else if (SceneManager.GetActiveScene().name.Equals("CaveScene"))
            {
                gameManager.HideExitCaveHintText();
            }
            if(gameManager.GetControlPlayerName() == GameManager.ControlCharacter.ElderBrother)
            {
                PlayerFollowCamera.Follow = ElderBrotherCharacterCameraRoot.transform;
            }
            else
            {
                PlayerFollowCamera.Follow = YoungerBroherCharacterCameraRoot.transform;
            }
        }
    }
}
