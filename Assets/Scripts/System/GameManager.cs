using Cinemachine;
using StarterAssets;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance
    {
        get; private set;
    }

    public enum ControlCharacter
    {
        ElderBrother,
        YoungerBrother
    }

    public enum GameState
    {
        CountDown,
        GameStart,
        ShowHints,
        FoundByEnemy,
        Win,
        GameOver,
    }

    public RenderTexture bottleRenderTexture, ladderRenderTexture;
    public GameObject bottleItem, ladderItem;
    public string cannotCollectWaterDropText;

    [SerializeField]private GameObject currentBottleItem, currentLadderItem;
    private GameObject targetLadder;
    private GameObject AnkaDialogCanvas;
    private AudioSource audioSource;
    [SerializeField] private Image AnkaDialogPanel, AnkaDialogImage, waterDropInitialImage, waterDrop1Image, waterDrop2Image, waterDrop3Image;
    private RawImage bottleItemRenderTexture;
    private Image ladderItemImage;
    private Text attackEnemyHint, climbHint, useLadderItemHintText, pickItemHint, useTrapHint, attractEnemyHint, attractEnemyEndHint;
    private Text cannotStandUpHereHintText, soloActionOnlyHintText, soloActionStartHintText, soloActionEndHintText, enterCaveHint, exitCaveHint, openDoorHintText;
    private Text AnkaDialogText;
    private GameState currentGameState;
    private static bool m_IsInSoloAction;
    private static bool m_ExitFromCave;
    private static bool m_InBackMountain;
    private static bool m_EnteredFromBackMountain;
    private static bool m_RestartAtCheckPointFromCave;
    private static bool m_IsBottlePicked;
    private static bool m_HasBottle;
    private static bool m_DestroyBigStone;
    private static bool m_ClosetDestroyed;
    private static bool m_WaterDrop1Collected, m_WaterDrop2Collected, m_WaterDrop3Collected;
    private static bool m_Ladder1Repaired, m_Ladder2Repaired;
    private static bool m_IsEnemy1Died, m_IsEnemy3Died, m_IsEnemy11Died, m_IsEnemy12Died;
    private static bool m_IsHint1Showed, m_IsHint2Showed, m_IsHint3Showed, m_IsHint4Showed, m_IsHint5Showed, m_IsHint6Showed;
    private static bool m_IsHint8Showed, m_IsHint9Showed, m_IsHint10Showed, m_IsHint10ver2Showed, m_IsHint13Showed, m_IsHint14Showed, m_IsHint15Showed;
    private static bool m_IsHint16Showed, m_IsHint17Showed, m_IsHint18Showed;
    private static ControlCharacter controlPlayerName;
    private static float ElderBrotherPositionX, ElderBrotherPositionY, ElderBrotherPositionZ;
    private static float ElderBrotherRotationX, ElderBrotherRotationY, ElderBrotherRotationZ;
    private static float YoungerBrotherPositionX, YoungerBrotherPositionY, YoungerBrotherPositionZ;
    private static float YoungerBrotherRotationX, YoungerBrotherRotationY, YoungerBrotherRotationZ;
    private static float BirdPositionX, BirdPositionY, BirdPositionZ;
    private static float BirdRotationX, BirdRotationY, BirdRotationZ;
    private static string enterCaveName, lastCheckPointName;
    private static string _controlCharacter;
    [SerializeField] private int waterDropCount;

    // Start is called before the first frame update
    void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }
        Instance = this;
        Initialization();
    }

    private void Initialization()
    {
        AnkaDialogCanvas = GameObject.Find("AnkaDialogCanvas");
        AnkaDialogText = GameObject.Find("AnkaDialogText").GetComponent<Text>();
        AnkaDialogPanel = GameObject.Find("AnkaDialogPanel").GetComponent<Image>();
        attackEnemyHint = GameObject.Find("AttackEnemyHintText").GetComponent<Text>();
        attackEnemyHint.enabled = false;
        climbHint = GameObject.Find("ClimbHintText").GetComponent<Text>();
        climbHint.enabled = false;
        useLadderItemHintText = GameObject.Find("UseLadderItemHintText").GetComponent<Text>();
        useLadderItemHintText.enabled = false;
        pickItemHint = GameObject.Find("PickItemHintText").GetComponent<Text>();
        pickItemHint.enabled = false;
        useTrapHint = GameObject.Find("UseTrapHintText").GetComponent<Text>();
        useTrapHint.enabled = false;
        attractEnemyHint = GameObject.Find("AttractEnemyHintText").GetComponent<Text>();
        attractEnemyHint.enabled = false;
        attractEnemyEndHint = GameObject.Find("AttractEnemyEndHintText").GetComponent<Text>();
        attractEnemyEndHint.enabled = false;
        cannotStandUpHereHintText = GameObject.Find("CannotStandUpHintText").GetComponent<Text>();
        cannotStandUpHereHintText.enabled = false;
        soloActionOnlyHintText = GameObject.Find("SoloActionOnlyHintText").GetComponent<Text>();
        soloActionOnlyHintText.enabled = false;
        soloActionStartHintText = GameObject.Find("SoloActionStartHintText").GetComponent<Text>();
        soloActionStartHintText.enabled = false;
        soloActionEndHintText = GameObject.Find("SoloActionEndHintText").GetComponent<Text>();
        soloActionEndHintText.enabled = false;
        enterCaveHint = GameObject.Find("EnterCaveHintText").GetComponent<Text>();
        enterCaveHint.enabled = false;
        exitCaveHint = GameObject.Find("ExitCaveHintText").GetComponent<Text>();
        exitCaveHint.enabled = false;
        bottleItemRenderTexture = GameObject.Find("BottleItemImage").GetComponent<RawImage>();
        bottleItemRenderTexture.enabled = false;
        ladderItemImage = GameObject.Find("LadderItemImage").GetComponent<Image>();
        ladderItemImage.enabled = false;
        currentGameState = GameState.CountDown;
        controlPlayerName = ControlCharacter.ElderBrother;
        m_ExitFromCave = false;
        m_InBackMountain = false;
        m_EnteredFromBackMountain = false;
        m_RestartAtCheckPointFromCave = true;
        m_IsBottlePicked = false;
        m_HasBottle = false;
        m_DestroyBigStone = false;
        m_ClosetDestroyed = false;
        waterDropCount = 0;
        waterDropInitialImage = GameObject.Find("WaterDropInitialImage").GetComponent<Image>();
        waterDrop1Image = GameObject.Find("WaterDrop1Image").GetComponent<Image>();
        waterDrop2Image = GameObject.Find("WaterDrop2Image").GetComponent<Image>();
        waterDrop3Image = GameObject.Find("WaterDrop3Image").GetComponent<Image>();
        openDoorHintText = GameObject.Find("OpenDoorHintText").GetComponent<Text>();
        openDoorHintText.enabled = false;
        currentBottleItem = null;
        currentLadderItem = null;
        m_WaterDrop1Collected = false;
        m_WaterDrop2Collected = false;
        m_WaterDrop3Collected = false;
        m_Ladder1Repaired = false;
        m_Ladder2Repaired = false;
        m_IsEnemy1Died = false;
        m_IsEnemy3Died = false;
        m_IsEnemy11Died = false;
        m_IsEnemy12Died = false;
        m_IsHint1Showed = false;
        m_IsHint2Showed = false;
        m_IsHint3Showed = false;
        m_IsHint4Showed = false;
        m_IsHint5Showed = false;
        m_IsHint6Showed = false;
        m_IsHint8Showed = false;
        m_IsHint9Showed = false;
        m_IsHint10Showed = false;
        m_IsHint10ver2Showed = false;
        m_IsHint13Showed = false;
        m_IsHint14Showed = false;
        m_IsHint15Showed = false;
        m_IsHint16Showed = false;
        m_IsHint17Showed = false;
        m_IsHint18Showed = false;
        m_IsInSoloAction = false;
        lastCheckPointName = "";

        DontDestroyOnLoad(this);
    }

    public void GetAnkaDialog()
    {
        if( AnkaDialogCanvas == null)
        {
            AnkaDialogCanvas = GameObject.Find("AnkaDialogCanvas");
        }
        if (AnkaDialogPanel == null)
        {
            AnkaDialogPanel = GameObject.Find("AnkaDialogPanel").GetComponent<Image>();
        }
        if (AnkaDialogImage == null)
        {
            AnkaDialogImage = GameObject.Find("AnkaDialogImage").GetComponent<Image>();
        }
    }

    public GameState GetGameState()
    {
        return currentGameState;
    }

    public void SetGameState(GameState targetGameState)
    {
        currentGameState = targetGameState;
    }

    public void EnemyDied(string enemyName)
    {
        switch (enemyName)
        {
            case "Enemy1":
                m_IsEnemy1Died = true;
                break;
            case "Enemy3":
                m_IsEnemy3Died = true;
                break;
            case "Enemy11":
                m_IsEnemy11Died = true;
                break;
            case "Enemy12":
                m_IsEnemy12Died = true;
                break;
        }
    }

    public bool IsEnemy1Died()
    {
        return m_IsEnemy1Died;
    }

    public bool IsEnemy3Died()
    {
        return m_IsEnemy3Died;
    }

    public bool IsEnemy11Died()
    {
        return m_IsEnemy11Died;
    }
    public bool IsEnemy12Died()
    {
        return m_IsEnemy12Died;
    }

    public void ResetAllEnemiesToAlive()
    {
        m_IsEnemy1Died = false;
        m_IsEnemy3Died = false;
        m_IsEnemy11Died = false;
        m_IsEnemy12Died = false;
    }

    public void SetOnlyYoungerBrotherAction(bool soloActionFlag)
    {
        m_IsInSoloAction = soloActionFlag;
    }

    public bool IsOnlyYoungerBrotherAction()
    {
        return m_IsInSoloAction;
    }

    public void PickedBottle()
    {
        m_IsBottlePicked = true;
    }

    public bool IsBottlePicked()
    {
        return m_IsBottlePicked;
    }

    public bool HasBottle()
    {
        return m_HasBottle;
    }

    public void ActivateDestroyBigStone()
    {
        m_DestroyBigStone = true;
    }

    public void ActivateDestroyCloset()
    {
        m_ClosetDestroyed = true;
    }

    public void LadderGetRepaired(string ladderName)
    {
        switch (ladderName)
        {
            case "Ladder1":
                m_Ladder1Repaired = true;
                break;
            case "Ladder2":
                m_Ladder2Repaired = true;
                break;
        }
    }

    public bool IsLadder1Repaired()
    {
        return m_Ladder1Repaired;
    }

    public bool IsLadder2Repaired()
    {
        return m_Ladder2Repaired;
    }

    public bool IsBigStoneDestroyed()
    {
        return m_DestroyBigStone;
    }

    public bool IsClosetDestroyed()
    {
        return m_ClosetDestroyed;
    }

    public void SetHintFlagShowed(string hintObjectName)
    {
        switch (hintObjectName)
        {
            case "Hint1Trigger":
                m_IsHint1Showed = true;
                break;
            case "Hint2Trigger":
                m_IsHint2Showed = true;
                break;
            case "Hint3Trigger":
                m_IsHint3Showed = true;
                break;
            case "Hint4Trigger":
                m_IsHint4Showed = true;
                break;
            case "Hint5Trigger":
                m_IsHint5Showed = true;
                break;
            case "Hint6Trigger":
                m_IsHint6Showed = true;
                break;
            case "Hint8Trigger":
                m_IsHint8Showed = true;
                break;
            case "Hint9Trigger":
                m_IsHint9Showed = true;
                break;
            case "Hint10Trigger1":
                m_IsHint10Showed = true;
                break;
            case "Hint10Trigger2":
                m_IsHint10ver2Showed = true;
                break;
            case "Hint13Trigger":
                m_IsHint13Showed = true;
                break;
            case "Hint14Trigger":
                m_IsHint14Showed = true;
                break;
            case "Hint15Trigger":
                m_IsHint15Showed = true;
                break;
            case "Hint16Trigger":
                m_IsHint16Showed = true;
                break;
            case "Hint17Trigger":
                m_IsHint17Showed = true;
                break;
            case "Hint18Trigger":
                m_IsHint18Showed = true;
                break;
        }
    }

    public bool IsHintShowed(string hintObjectName)
    {
        switch (hintObjectName)
        {
            case "Hint1Trigger":
                return m_IsHint1Showed;
            case "Hint2Trigger":
                return m_IsHint2Showed;
            case "Hint3Trigger":
                return m_IsHint3Showed;
            case "Hint4Trigger":
                return m_IsHint4Showed;
            case "Hint5Trigger":
                return m_IsHint5Showed;
            case "Hint6Trigger":
                return m_IsHint6Showed;
            case "Hint8Trigger":
                return m_IsHint8Showed;
            case "Hint9Trigger":
                return m_IsHint9Showed;
            case "Hint10Trigger1":
                return m_IsHint10Showed;
            case "Hint10Trigger2":
                return m_IsHint10ver2Showed;
            case "Hint13Trigger":
                return m_IsHint13Showed;
            case "Hint14Trigger":
                return m_IsHint14Showed;
            case "Hint15Trigger":
                return m_IsHint15Showed;
            case "Hint16Trigger":
                return m_IsHint16Showed;
            case "Hint17Trigger":
                return m_IsHint17Showed;
            case "Hint18Trigger":
                return m_IsHint18Showed;
        }
        return false;
    }

    public void ResetAllHintTriggers()
    {
        m_IsHint1Showed = false;
        m_IsHint2Showed = false;
        m_IsHint3Showed = false;
        m_IsHint4Showed = false;
        m_IsHint5Showed = false;
        m_IsHint8Showed = false;
        m_IsHint9Showed = false;
        m_IsHint10Showed = false;
        m_IsHint10ver2Showed = false;
        m_IsHint13Showed = false;
        m_IsHint14Showed = false;
        m_IsHint15Showed = false;
        m_IsHint16Showed = false;
        m_IsHint17Showed = false;
        m_IsHint18Showed = false;
    }

    public void ChangeControlPlayerName(ControlCharacter playerName)
    {
        controlPlayerName = playerName;
    }

    public ControlCharacter GetControlPlayerName()
    {
        return controlPlayerName;
    }

    public void SaveMainSceneCharacterPositions(Transform ElderBrotherTransform, Transform YoungerBrotherTransform, Transform BirdTransform)
    {
        ElderBrotherPositionX = ElderBrotherTransform.position.x;
        ElderBrotherPositionY = ElderBrotherTransform.position.y;
        ElderBrotherPositionZ = ElderBrotherTransform.position.z;
        ElderBrotherRotationX = ElderBrotherTransform.eulerAngles.x;
        ElderBrotherRotationY = ElderBrotherTransform.eulerAngles.y;
        ElderBrotherRotationZ = ElderBrotherTransform.eulerAngles.z;
        
        YoungerBrotherPositionX = YoungerBrotherTransform.position.x;
        YoungerBrotherPositionY = YoungerBrotherTransform.position.y;
        YoungerBrotherPositionZ = YoungerBrotherTransform.position.z;
        YoungerBrotherRotationX = YoungerBrotherTransform.eulerAngles.x;
        YoungerBrotherRotationY = YoungerBrotherTransform.eulerAngles.y;
        YoungerBrotherRotationZ = YoungerBrotherTransform.eulerAngles.z;
        
        BirdPositionX = BirdTransform.position.x;
        BirdPositionY = BirdTransform.position.y;
        BirdPositionZ = BirdTransform.position.z;
        BirdRotationX = BirdTransform.eulerAngles.x;
        BirdRotationY = BirdTransform.eulerAngles.y;
        BirdRotationZ = BirdTransform.eulerAngles.z;
    }

    public void UpdateLastCheckPointName(string checkPointName)
    {
        lastCheckPointName = checkPointName;
    }

    public string GetLastCheckPointName()
    {
        return lastCheckPointName;
    }

    public Vector3 GetMainSceneElderBrotherPosition()
    {
        return new Vector3(ElderBrotherPositionX, ElderBrotherPositionY, ElderBrotherPositionZ);
    }

    public Quaternion GetMainSceneElderBrotherRotation()
    {
        return Quaternion.Euler(ElderBrotherRotationX, ElderBrotherRotationY, ElderBrotherRotationZ);
    }

    public Vector3 GetMainSceneYoungerBrotherPosition()
    {
        return new Vector3(YoungerBrotherPositionX, YoungerBrotherPositionY, YoungerBrotherPositionZ);
    }

    public Quaternion GetMainSceneYoungerBrotherRotation()
    {
        return Quaternion.Euler(YoungerBrotherRotationX, YoungerBrotherRotationY, YoungerBrotherRotationZ);
    }

    public Vector3 GetMainSceneBirdPosition()
    {
        return new Vector3(BirdPositionX, BirdPositionY, BirdPositionZ);
    }

    public Quaternion GetMainSceneBirdRotation()
    {
        return Quaternion.Euler(BirdRotationX, BirdRotationY, BirdRotationZ);
    }

    public void EnterCave()
    {
        m_ExitFromCave = false;
    }

    public void ExitCave()
    {
        m_ExitFromCave = true;
    }

    public bool IsExitFromCave()
    {
        return m_ExitFromCave;
    }

    public bool CanUseBottle()
    {
        return currentBottleItem != null;
    }

    public bool CanUseLadder()
    {
        return currentLadderItem != null;
    }

    public void UseLadderItem()
    {
        targetLadder.GetComponent<Ladders>().RepairLadder();
        currentLadderItem = null;
        FindObjectOfType<ThirdPersonController>().DisableUseItemFlag();
        ladderItemImage.enabled = false;
        FindObjectOfType<GameManager>().HideUseItemHint();
    }

    public void PickItem(string itemName)
    {
        switch (itemName)
        {
            case "Bottle":
                currentBottleItem = bottleItem;
                if(bottleItemRenderTexture == null)
                {
                    bottleItemRenderTexture = GameObject.Find("BottleItemImage").GetComponent<RawImage>();
                }
                bottleItemRenderTexture.texture = bottleRenderTexture;
                if (bottleItemRenderTexture.texture != null)
                {
                    bottleItemRenderTexture.enabled = true;
                }
                m_HasBottle = true;
                break;
            case "Ladder":
                currentLadderItem = ladderItem;
                if (ladderItemImage == null)
                {
                    ladderItemImage = GameObject.Find("LadderItemImage").GetComponent<Image>();
                }
                ladderItemImage.enabled = true;
                break;
        }
    }

    public void SetTargetLadder(GameObject ladder)
    {
        targetLadder = ladder;
    }

    public bool HasTargetLadder()
    {
        return targetLadder != null;
    }

    public void ShowAttackEnemyHint()
    {
        if (attackEnemyHint == null)
        {
            attackEnemyHint = GameObject.Find("AttackEnemyHintText").GetComponent<Text>();
        }
        attackEnemyHint.enabled = true;
    }

    public void HideAttackEnemyHint()
    {
        if (attackEnemyHint == null)
        {
            attackEnemyHint = GameObject.Find("AttackEnemyHintText").GetComponent<Text>();
        }
        attackEnemyHint.enabled = false;
    }

    public void ShowClimbHint(string hint = "")
    {
        if (climbHint == null)
        {
            climbHint = GameObject.Find("ClimbHintText").GetComponent<Text>();
        }
        if (!hint.Equals(""))
        {
            climbHint.text = hint;
        }
        else
        {
            climbHint.text = "E: Climb";
        }
        climbHint.enabled = true;
    }

    public void HideClimbHint()
    {
        if (climbHint == null)
        {
            climbHint = GameObject.Find("ClimbHintText").GetComponent<Text>();
        }
        climbHint.enabled = false;
    }

    public void ShowUseItemHint()
    {
        if (useLadderItemHintText == null)
        {
            useLadderItemHintText = GameObject.Find("UseLadderItemHintText").GetComponent<Text>();
        }
        useLadderItemHintText.enabled = true;
    }

    public void HideUseItemHint()
    {
        if (useLadderItemHintText == null)
        {
            useLadderItemHintText = GameObject.Find("UseLadderItemHintText").GetComponent<Text>();
        }
        useLadderItemHintText.enabled = false;
    }

    public void ShowPickItemHint()
    {
        if (pickItemHint == null)
        {
            pickItemHint = GameObject.Find("PickItemHintText").GetComponent<Text>();
        }
        pickItemHint.enabled = true;
    }

    public void HidePickItemHint()
    {
        if (pickItemHint == null)
        {
            pickItemHint = GameObject.Find("PickItemHintText").GetComponent<Text>();
        }
        pickItemHint.enabled = false;
    }

    public void ShowCannotCollectWaterDropHint()
    {
        if (AnkaDialogCanvas == null)
        {
            AnkaDialogCanvas = GameObject.Find("AnkaDialogCanvas");
        }
        AnkaDialogCanvas.SetActive(true);
        if (AnkaDialogPanel == null)
        {
            AnkaDialogPanel = GameObject.Find("AnkaDialogPanel").GetComponent<Image>();
        }
        if (AnkaDialogImage == null)
        {
            AnkaDialogImage = GameObject.Find("AnkaDialogImage").GetComponent<Image>();
        }
        if (AnkaDialogText == null)
        {
            AnkaDialogText = GameObject.Find("AnkaDialogText").GetComponent<Text>();
        }
        AnkaDialogPanel.enabled = true;
        AnkaDialogPanel.color = new Color(AnkaDialogPanel.color.r, AnkaDialogPanel.color.b, AnkaDialogPanel.color.b, 0f);
        AnkaDialogImage.enabled = true;
        AnkaDialogText.enabled = true;
        AnkaDialogText.text = cannotCollectWaterDropText;
    }

    public void HideCannotCollectWaterDropHint()
    {
        if (AnkaDialogCanvas == null)
        {
            AnkaDialogCanvas = GameObject.Find("AnkaDialogCanvas");
        }
        AnkaDialogCanvas.SetActive(false);
    }


    public void ShowUseTrapHint()
    {
        if (useTrapHint == null)
        {
            useTrapHint = GameObject.Find("UseTrapHintText").GetComponent<Text>();
        }
        useTrapHint.enabled = true;
    }

    public void HideUseTrapHint()
    {
        if (useTrapHint == null)
        {
            useTrapHint = GameObject.Find("UseTrapHintText").GetComponent<Text>();
        }
        useTrapHint.enabled = false;
    }

    public void ShowAttractEnemyHint()
    {
        if (attractEnemyHint == null)
        {
            attractEnemyHint = GameObject.Find("AttractEnemyHintText").GetComponent<Text>();
        }
        attractEnemyHint.enabled = true;
    }

    public void HideAttractEnemyHint()
    {
        if (attractEnemyHint == null)
        {
            attractEnemyHint = GameObject.Find("AttractEnemyHintText").GetComponent<Text>();
        }
        attractEnemyHint.enabled = false;
    }

    public void ShowAttractEnemyEndHint()
    {
        if (attractEnemyEndHint == null)
        {
            attractEnemyEndHint = GameObject.Find("AttractEnemyEndHintText").GetComponent<Text>();
        }
        attractEnemyEndHint.enabled = true;
    }

    public void HideAttractEnemyEndHint()
    {
        if (attractEnemyEndHint == null)
        {
            attractEnemyEndHint = GameObject.Find("AttractEnemyEndHintText").GetComponent<Text>();
        }
        attractEnemyEndHint.enabled = false;
    }

    public void ShowCannotStandUpHereHintText()
    {
        if (cannotStandUpHereHintText == null)
        {
            cannotStandUpHereHintText = GameObject.Find("CannotStandUpHereHintText").GetComponent<Text>();
        }
        cannotStandUpHereHintText.enabled = true;
    }

    public void HideCannotStandUpHereHintText()
    {
        if (cannotStandUpHereHintText == null)
        {
            cannotStandUpHereHintText = GameObject.Find("CannotStandUpHereHintText").GetComponent<Text>();
        }
        cannotStandUpHereHintText.enabled = false;
    }
    public void ShowSoloActionOnlyHintText()
    {
        if (soloActionOnlyHintText == null)
        {
            soloActionOnlyHintText = GameObject.Find("SoloActionOnlyHintText").GetComponent<Text>();
        }
        soloActionOnlyHintText.enabled = true;
    }

    public void HideSoloActionOnlyHintText()
    {
        if (soloActionOnlyHintText == null)
        {
            soloActionOnlyHintText = GameObject.Find("SoloActionOnlyHintText").GetComponent<Text>();
        }
        soloActionOnlyHintText.enabled = false;
    }

    public void ShowSoloActionStartHintText()
    {
        if (soloActionStartHintText == null)
        {
            soloActionStartHintText = GameObject.Find("SoloActionStartHintText").GetComponent<Text>();
        }
        soloActionStartHintText.enabled = true;
    }

    public void HideSoloActionStartHintText()
    {
        if (soloActionStartHintText == null)
        {
            soloActionStartHintText = GameObject.Find("SoloActionStartHintText").GetComponent<Text>();
        }
        soloActionStartHintText.enabled = false;
    }

    public void ShowSoloActionEndHintText()
    {
        if (soloActionEndHintText == null)
        {
            soloActionEndHintText = GameObject.Find("SoloActionEndHintText").GetComponent<Text>();
        }
        soloActionEndHintText.enabled = true;
    }

    public void HideSoloActionEndHintText()
    {
        if (soloActionEndHintText == null)
        {
            soloActionEndHintText = GameObject.Find("SoloActionEndHintText").GetComponent<Text>();
        }
        soloActionEndHintText.enabled = false;
    }

    public void ShowEnterCaveHintText()
    {
        if (enterCaveHint == null)
        {
            enterCaveHint = GameObject.Find("EnterCaveHintText").GetComponent<Text>();
        }
        enterCaveHint.enabled = true;
    }

    public void HideEnterCaveHintText()
    {
        if (enterCaveHint == null)
        {
            enterCaveHint = GameObject.Find("EnterCaveHintText").GetComponent<Text>();
        }
        enterCaveHint.enabled = false;
    }

    public void ShowExitCaveHintText()
    {
        if (exitCaveHint == null)
        {
            exitCaveHint = GameObject.Find("ExitCaveHintText").GetComponent<Text>();
        }
        exitCaveHint.enabled = true;
    }

    public void HideExitCaveHintText()
    {
        if (exitCaveHint == null)
        {
            exitCaveHint = GameObject.Find("ExitCaveHintText").GetComponent<Text>();
        }
        exitCaveHint.enabled = false;
    }

    public void ShowOpenDoorHintText()
    {
        if (openDoorHintText == null)
        {
            openDoorHintText = GameObject.Find("OpenDoorHintText").GetComponent<Text>();
        }
        openDoorHintText.enabled = true;
    }

    public void HideOpenDoorHintText()
    {
        if (openDoorHintText == null)
        {
            openDoorHintText = GameObject.Find("OpenDoorHintText").GetComponent<Text>();
        }
        openDoorHintText.enabled = false;
    }

    public void SetEnterCaveAreaName(string areaName)
    {
        enterCaveName = areaName;
    }

    public string GetEnterCaveAreaName()
    {
        return enterCaveName;
    }

    public void CollectedWaterDrop(string waterDropName)
    {
        waterDropCount++;

        switch (waterDropName)
        {
            case "WaterDrop1":
                m_WaterDrop1Collected = true;
                break;
            case "WaterDrop2":
                m_WaterDrop2Collected = true;
                break;
            case "WaterDrop3":
                m_WaterDrop3Collected = true;
                break;
        }
    }

    public void ShowWaterDropImage()
    {
        if (waterDropInitialImage == null)
        {
            waterDropInitialImage = GameObject.Find("WaterDropInitialImage").GetComponent<Image>();
        }

        if (waterDrop1Image == null)
        {
            waterDrop1Image = GameObject.Find("WaterDrop1Image").GetComponent<Image>();
        }

        if (waterDrop2Image == null)
        {
            waterDrop2Image = GameObject.Find("WaterDrop2Image").GetComponent<Image>();
        }

        if (waterDrop3Image == null)
        {
            waterDrop3Image = GameObject.Find("WaterDrop3Image").GetComponent<Image>();
        }

        switch (waterDropCount)
        {
            case 0:
                waterDropInitialImage.enabled = true;
                waterDrop1Image.enabled = false;
                waterDrop2Image.enabled = false;
                waterDrop3Image.enabled = false;
                break;
            case 1:
                waterDropInitialImage.enabled = false;
                waterDrop1Image.enabled = true;
                waterDrop2Image.enabled = false;
                waterDrop3Image.enabled = false;
                break;
            case 2:
                waterDropInitialImage.enabled = false;
                waterDrop1Image.enabled = false;
                waterDrop2Image.enabled = true;
                waterDrop3Image.enabled = false;
                break;
            case 3:
                waterDropInitialImage.enabled = false;
                waterDrop1Image.enabled = false;
                waterDrop2Image.enabled = false;
                waterDrop3Image.enabled = true;
                break;
        }
    }

    public bool IsWaterDrop1Collected()
    {
        return m_WaterDrop1Collected;
    }

    public bool IsWaterDrop2Collected()
    {
        return m_WaterDrop2Collected;
    }

    public bool IsWaterDrop3Collected()
    {
        return m_WaterDrop3Collected;
    }

    public int GetWaterDropCount()
    {
        return waterDropCount;
    }

    public void GoToBackMountain()
    {
        m_InBackMountain = true;
    }

    public void ExitFromBackMountain()
    {
        m_InBackMountain = false;
    }

    public bool IsInBackMountain()
    {
        return m_InBackMountain;
    }

    public void EnterFromBackMountain()
    {
        m_EnteredFromBackMountain = true;
    }

    public void NotEnterFromBackMountain()
    {
        m_EnteredFromBackMountain = false;
    }

    public bool IsEnteredFromBackMountain()
    {
        return m_EnteredFromBackMountain;
    }

    public void RestartAtCheckPointFromCave()
    {
        m_RestartAtCheckPointFromCave = true;
    }

    public void UnsetRestartAtCheckPointFromCave()
    {
        m_RestartAtCheckPointFromCave = false;
    }

    public bool IsRestartAtCheckPointFromCave()
    {
        return m_RestartAtCheckPointFromCave;
    }

    public void HideAnkaDialog()
    {
        if (AnkaDialogCanvas == null)
        {
            AnkaDialogCanvas = GameObject.Find("AnkaDialogCanvas");
        }
        AnkaDialogCanvas.SetActive(false);
    }

    public void Win()
    {
        FindObjectOfType<ThirdPersonController>().enabled = false;
        foreach(PlayerInput p in FindObjectsOfType<PlayerInput>())
        {
            p.enabled = false;
        }
        InputSystem.EnableDevice(Mouse.current);
        FindObjectOfType<CaveManager>().ToEndingScreen();
    }
}
