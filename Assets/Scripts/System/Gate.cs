using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    public float endingSceneEffectSeconds = 3f, destroyEndingSceneEffectSeconds = 3f;
    public Transform endingSceneEffectPlace;
    public ParticleSystem endingSceneEffect;

    public void GameClear()
    {
        if (endingSceneEffect != null && endingSceneEffectPlace != null)
        {
            StartCoroutine(PlayEndingSceneEffect());
        }
        else
        {
            FindObjectOfType<GameManager>().Win();
        }
    }

    private IEnumerator PlayEndingSceneEffect()
    {
        ParticleSystem newEndingSceneEffet = Instantiate(endingSceneEffect);
        newEndingSceneEffet.transform.position = endingSceneEffectPlace.position;
        newEndingSceneEffet.transform.rotation = Quaternion.identity;
        newEndingSceneEffet.Play();
        Destroy(newEndingSceneEffet, destroyEndingSceneEffectSeconds);

        yield return new WaitForSeconds(endingSceneEffectSeconds);

        FindObjectOfType<GameManager>().Win();
    }
}
