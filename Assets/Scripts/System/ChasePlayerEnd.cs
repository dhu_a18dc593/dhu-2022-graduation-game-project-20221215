using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayerEnd : MonoBehaviour
{
    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            FindObjectOfType<MainSceneManager>().EnemiesEndOfChasingPlayer();
        }
    }
}
