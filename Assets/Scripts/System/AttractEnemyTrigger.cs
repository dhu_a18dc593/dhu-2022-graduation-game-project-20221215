using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttractEnemyTrigger : MonoBehaviour
{
    public GameObject attractEnemiesTargetPos, attractEnemiesFacePos;
    public Enemy[] enemies;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name.Equals("ElderBrother") && enemies.Length > 0)
        {
            FindObjectOfType<AnimalCharacterAI>().SetAttractEnemiesPositionAndFacePosition(attractEnemiesTargetPos, attractEnemiesFacePos);
        }
    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.name.Equals("ElderBrother") && enemies.Length > 0 && FindObjectOfType<AnimalCharacterAI>().HasAttractEnemiesPosition())
        {
            collider.gameObject.GetComponent<ThirdPersonController>().EnableAttractEnemyFlag();
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name.Equals("ElderBrother"))
        {
            if (FindObjectOfType<AnimalCharacterAI>().IsAttractingEnemies())
            {
                FindObjectOfType<AnimalCharacterAI>().EndOfAttractingEnemies();
                foreach(Enemy e in enemies)
                {
                    e.StopAttractedByBird();
                }
            }
            else
            {
                FindObjectOfType<AnimalCharacterAI>().UnSetAttractEnemiesPosition();
            }
            collider.gameObject.GetComponent<ThirdPersonController>().DisableAttractEnemyFlag();
        }
    }
}
