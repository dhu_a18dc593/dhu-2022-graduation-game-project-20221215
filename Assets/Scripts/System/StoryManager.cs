using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoryManager : MonoBehaviour
{
    public Slider loadingSlider;
    public float loadSceneTime = 5f;

    private void Awake()
    {
        StartCoroutine(LoadLevelAsynchronously("MainScene"));
    }

    IEnumerator LoadLevelAsynchronously(string targetMap)
    {
        yield return new WaitForSeconds(loadSceneTime);

        AsyncOperation operation = SceneManager.LoadSceneAsync(targetMap);
        operation.allowSceneActivation = false;

        while (operation.progress < 0.9f)
        {
            if(loadingSlider != null)
            {
                loadingSlider.value = operation.progress;
            }

            yield return 0;
        }
        operation.allowSceneActivation = true;
        loadingSlider.value = 1f;
        yield return operation;
    }
}
