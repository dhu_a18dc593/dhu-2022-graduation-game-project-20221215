using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTrigger : MonoBehaviour
{
    public GameObject targetItem;
    public Transform itemPointPosition, itemPlacePosition, dropItemPosition;
    public string hint;

    [SerializeField] private bool isItemPicked;

    private void Update()
    {
        if (targetItem != null && targetItem.tag.Equals("Bottle"))
        {
            CheckBottleIsPicked();
        }
    }

    private void CheckBottleIsPicked()
    {
        if (FindObjectOfType<GameManager>().IsBottlePicked())
        {
            isItemPicked = true;
        }
    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Bird") && targetItem != null)
        {
            FindObjectOfType<ThirdPersonController>().ActivatePickItemFlag();
            collider.gameObject.GetComponent<AnimalCharacterAI>().SetTargetItem(targetItem);
            collider.gameObject.GetComponent<AnimalCharacterAI>().SetGetItemPointPosittion(itemPointPosition, itemPlacePosition, dropItemPosition);
            if (!isItemPicked)
            {
                FindObjectOfType<GameManager>().ShowPickItemHint();
            }
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Bird"))
        {
            FindObjectOfType<ThirdPersonController>().DeactivatePickItemFlag();
            //collider.gameObject.GetComponent<AnimalCharacterAI>().UnsetTargetItem();
            //collider.gameObject.GetComponent<AnimalCharacterAI>().UnsetGetItemPointPosittion();
            FindObjectOfType<GameManager>().HidePickItemHint();
        }
    }
}
