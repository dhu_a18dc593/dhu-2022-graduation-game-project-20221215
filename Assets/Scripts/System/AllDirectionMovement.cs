using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllDirectionMovement : MonoBehaviour {
    // 2022-06-15 by 楊
    // プレイヤーが特定エリアに上下移動で隠れることができます

    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            collider.gameObject.GetComponent<ThirdPersonController>().EnableAllDirectionMovementFlag();
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            collider.gameObject.GetComponent<ThirdPersonController>().DisbbleAllDirectionMovementFlag();
        }
    }
}
