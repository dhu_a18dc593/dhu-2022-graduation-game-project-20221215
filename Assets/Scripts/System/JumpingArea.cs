using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class JumpingArea : MonoBehaviour
{
    [SerializeField] private bool jumpToGroundFlag = false;

    // 2022-05-06 by 楊
    // プレイヤーは特定エリア内にジャンプできます
    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            collider.gameObject.GetComponent<ThirdPersonController>().EnableJump();
        }
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled &&
                gameObject.name.Equals("JumpingArea11") && !jumpToGroundFlag)
        {
            collider.gameObject.GetComponent<ThirdPersonController>().EnableJumpedToGroundFlag();
            jumpToGroundFlag = true;
        }
        else if (collider.gameObject.name.Equals("Chandelier"))
        {
            GameObject.Find("SoloActionArea2").GetComponent<SoloActionArea>().EnableEndOfSoloActionFlag();
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            ThirdPersonController player = collider.gameObject.GetComponent<ThirdPersonController>();
            player.DisableJump();
            if (gameObject.name.Equals("JumpingArea11"))
            {
                player.DisableJumpedToGroundFlag();
                jumpToGroundFlag = false;

                GameManager gameManager = FindObjectOfType<GameManager>();
                if (!gameManager.IsOnlyYoungerBrotherAction())
                {
                    FindObjectOfType<MainSceneManager>().RestoreAIJumpWaypointObject();
                }
            }
        }
    }
}
