using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackEnemyTrigger : MonoBehaviour
{
    public GameObject targetEnemy;

    private void OnTriggerStay(Collider collider)
    {
        if(collider.gameObject.gameObject.tag.Equals("Player") && 
            collider.gameObject.GetComponent<ThirdPersonController>().isActiveAndEnabled && targetEnemy != null)
        {
            if (collider.gameObject.name.Equals("ElderBrother"))
            {
                collider.gameObject.GetComponent<ThirdPersonController>().EnableAttackEnemyFlag();
                FindObjectOfType<GameManager>().ShowAttackEnemyHint();
                FindObjectOfType<AnimalCharacterAI>().SetTargetEnemy(targetEnemy);
            }
            else
            {
                collider.gameObject.GetComponent<ThirdPersonController>().DisableAttractEnemyFlag();
                FindObjectOfType<GameManager>().HideAttackEnemyHint();
                FindObjectOfType<AnimalCharacterAI>().UnsetTargetEnemy();
            }
        }
        else
        {
            collider.gameObject.GetComponent<ThirdPersonController>().DisableAttractEnemyFlag();
            FindObjectOfType<GameManager>().HideAttackEnemyHint();
            FindObjectOfType<AnimalCharacterAI>().UnsetTargetEnemy();
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.gameObject.tag.Equals("Player"))
        {
            FindObjectOfType<GameManager>().HideAttackEnemyHint();
            collider.gameObject.GetComponent<ThirdPersonController>().DisableAttractEnemyFlag();
        }
    }
}
