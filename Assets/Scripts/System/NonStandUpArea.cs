using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonStandUpArea : MonoBehaviour
{
    private void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player") && collider.gameObject.GetComponent<ThirdPersonController>().IsCrouching())
        {
            FindObjectOfType<GameManager>().ShowCannotStandUpHereHintText();
            collider.gameObject.GetComponent<ThirdPersonController>().ActvivateCannotStandUpFlag();
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Player"))
        {
            FindObjectOfType<GameManager>().HideCannotStandUpHereHintText();
            collider.gameObject.GetComponent<ThirdPersonController>().DeactivateCannotStandUpFlag();
        }
    }
}
