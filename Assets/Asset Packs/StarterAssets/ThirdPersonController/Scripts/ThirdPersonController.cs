﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
#endif

/* Note: animations are called via the controller for both the character and capsule using animator null checks
 */

namespace StarterAssets
{
    [RequireComponent(typeof(CharacterController))]
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
    [RequireComponent(typeof(PlayerInput))]
#endif
    public class ThirdPersonController : MonoBehaviour
    {
        [Header("Player")]
        public string PlayerName = "ElderBrother";

        [Tooltip("Slow down speed of the character in m/s")]
        public float SlowDownSpeed = 1f;

        [Tooltip("Move speed of the character in m/s")]
        public float MoveSpeed = 2.0f;

        [Tooltip("Sprint speed of the character in m/s")]
        public float SprintSpeed = 5.335f;

        [Tooltip("Crouch speed of the character in m/s")]
        public float CrouchSpeed = 1.75f;

        [Tooltip("How fast the character turns to face movement direction")]
        [Range(0.0f, 0.3f)]
        public float RotationSmoothTime = 0.12f;

        [Tooltip("Acceleration and deceleration")]
        public float SpeedChangeRate = 10.0f;

        public AudioClip LandingAudioClip;
        public AudioClip[] FootstepAudioClips;
        [Range(0, 1)] public float FootstepAudioVolume = 0.5f;

        [Space(10)]
        [Tooltip("The height the player can jump")]
        public float JumpHeight = 1.2f;

        [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
        public float Gravity = -15.0f;

        [Space(10)]
        [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
        public float JumpTimeout = 0.50f;

        [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
        public float FallTimeout = 0.15f;

        [Header("Player Grounded")]
        [Tooltip("If the character is grounded or not. Not part of the CharacterController built in grounded check")]
        public bool Grounded = true;

        [Tooltip("Useful for rough ground")]
        public float GroundedOffset = -0.14f;

        [Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
        public float GroundedRadius = 0.28f;

        [Tooltip("What layers the character uses as ground")]
        public LayerMask GroundLayers;

        [Header("Cinemachine")]
        [Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
        public GameObject CinemachineCameraTarget;
        public GameObject CinemachineCameraTarget2;

        public ThirdPersonController partnerCharacter;

        [Tooltip("How far in degrees can you move the camera up")]
        public float TopClamp = 70.0f;

        [Tooltip("How far in degrees can you move the camera down")]
        public float BottomClamp = -30.0f;

        [Tooltip("Additional degress to override the camera. Useful for fine tuning camera position when locked")]
        public float CameraAngleOverride = 0.0f;

        [Tooltip("For locking the camera position on all axis")]
        public bool LockCameraPosition = false;

        [Tooltip("敵の攻撃に与えられた時の硬直時間")]
        public float GetHitSeconds = 2.0f;

        [Tooltip("キャラクター自体のAudio Source")]
        public AudioSource characterAudioSource;

        [Tooltip("キャラクターの隠れルート")]
        public GameObject HideCharacterRoute;

        [Tooltip("隠れルートから戻る場所")]
        public GameObject BeforeHideCharacterRoutePosition;

        [Tooltip("キャラクターが梯子に登るスピード")]
        public float ClimbSpeed = 2.0f;

        [Tooltip("キャラクターが「LookAt」Ladder用")]
        public GameObject targetLadder;

        [Tooltip("キャラクターチェンジのCDタイム")]
        public float changeCharacterCDTime = 1.2f;

        [Tooltip("敵を攻撃するヒント")]
        public string attackEnemyHint = "T: Attack Enemy";

        [Tooltip("AIがしゃがむ遅延時間(s)。プレイヤーと同時にしゃがむをしないように設定する")]
        public float aiCrouchDelayTime = 0.3f;

        [Tooltip("AIがしゃがむ状態から起き上がる遅延時間(s)。プレイヤーと同時に起き上がることをしないように設定する")]
        public float aiStandUpDelayTime = 0.3f;

        public RuntimeAnimatorController animatorControllerPlayer;

        // cinemachine
        private float _cinemachineTargetYaw;
        private float _cinemachineTargetPitch;

        // player
        private float _speed;
        private float _animationBlend;
        private float _targetRotation = 0.0f;
        private float _rotationVelocity;
        private float _verticalVelocity;
        private float _terminalVelocity = 53.0f;

        // timeout deltatime
        private float _jumpTimeoutDelta;
        private float _fallTimeoutDelta;

        // animation IDs
        private int _animIDSpeed;
        private int _animIDGrounded;
        private int _animIDJump;
        private int _animIDFreeFall;
        private int _animIDMotionSpeed;
        private int _animIDCrouchWalk;

        // ジャンプする時キャラクター変更禁止
        // 指定するエリア以外隠れることを禁止する
        private static bool _canChangeCharacterFlag;
        private bool _canHideCharacterFlag;  // キャラクター隠れ用
        private float _startTime;
        private Vector3 _velocity = Vector3.zero;
        public float SmoothTime = 2f;

        private bool _canJump; // キャラクターは特定エリア以内にジャンプできます
        private static bool m_canClimb; // キャラクターは特定エリア以内にLadderに登れます
        private static bool m_canOpenDoor; // 最後のドアの開くフラグ
        private bool _canControl; // キャラクター操作フラグ
        private static bool _canActivateTrap; // トラップを発動させるフラグ
        private static bool _canAttackEnemy; // アンカに敵を攻撃させるフラグ
        private static bool m_isCrouching; // しゃがむできるフラグ & しゃがむ状態フラグ
        private static bool m_isStealthWalking; // ゆっくり歩く状態フラグ
        private static bool m_isWalking; // 歩く状態フラグ
        private static bool m_isRunning; // 走る状態フラグ
        private static bool _canUseItem; // アイテムを使うフラグ
        private static bool _canAttractEnemy;
        private static bool _cannotStandUpFlag;
        private static bool m_isInSoloAction;
        private static bool _canSoloAction;
        private static bool _canConjunction;
        private static bool m_canChangeScene;
        private static bool m_isCharacterChanging;
        private static bool m_JumpedToGround;
        [SerializeField] private BoxCollider climbTrigger;
        private static string climbTriggerName;

        private float targetSpeed;

#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
        private PlayerInput _playerInput;
#endif
        private Animator _animator;
        private CharacterController _controller;
        private StarterAssetsInputs _input;
        private GameObject _mainCamera;

        private const float _threshold = 0.01f;

        private bool _hasAnimator;

        // 2022-07-18 by 楊
        // OrderTakeItemを呼び出すように制限するフラグ
        [SerializeField] private static bool _canPickItem;


        protected bool IsCurrentDeviceMouse
        {
            get
            {
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
                return _playerInput.currentControlScheme == "KeyboardMouse";
#else
				return false;
#endif
            }
        }


        protected void Awake()
        {
            // get a reference to our main camera
            if (_mainCamera == null)
            {
                _mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
            }

            Initialization();
        }

        private void Initialization()
        {
            _cinemachineTargetYaw = CinemachineCameraTarget.transform.rotation.eulerAngles.y;

            _hasAnimator = TryGetComponent(out _animator);
            _controller = GetComponent<CharacterController>();
            _input = GetComponent<StarterAssetsInputs>();
#if ENABLE_INPUT_SYSTEM && STARTER_ASSETS_PACKAGES_CHECKED
            _playerInput = GetComponent<PlayerInput>();
#else
			Debug.LogError( "Starter Assets package is missing dependencies. Please use Tools/Starter Assets/Reinstall Dependencies to fix it");
#endif

            AssignAnimationIDs();

            // reset our timeouts on start
            _jumpTimeoutDelta = JumpTimeout;
            _fallTimeoutDelta = FallTimeout;
            _canChangeCharacterFlag = true;
            _canJump = false;
            m_canClimb = false;
            m_canOpenDoor = false;
            _canControl = true;
            _canHideCharacterFlag = false;
            _canPickItem = false;
            _canActivateTrap = false;
            m_isCharacterChanging = false;
            _canAttackEnemy = false;
            m_isCrouching = false;
            m_isStealthWalking = false;
            m_isWalking = false;
            m_isRunning = false;
            _canUseItem = false;
            _canAttractEnemy = false;
            _cannotStandUpFlag = false;
            m_isInSoloAction = false;
            _canSoloAction = false;
            _canConjunction = false;
            m_JumpedToGround = false;
            climbTrigger = null;
            climbTriggerName = "";
        }

        protected void Update()
        {
            _hasAnimator = TryGetComponent(out _animator);

            if(!_canControl) { return; }

            JumpAndGravity();
            GroundedCheck();
            Move();
            ProcessPickUpItem();
            ProcessChangeCharacter();
            ProcessAttackEnemy();
            ProcessUseItem();
        }

        public void SetClimbTrigger(BoxCollider trigger)
        {
            climbTrigger = trigger;
        }

        public void UnsetClimbTrigger()
        {
            climbTrigger = null;
        }

        public string GetClimbTriggerName()
        {
            return climbTrigger != null ? climbTrigger.name : "";
        }

        public void SetClimbTriggerName(string triggerName)
        {
            climbTriggerName = triggerName;
        }

        public void ProcessClimbLadder()
        {
            if(!m_canClimb) { return; }

            GameManager gameManager = FindObjectOfType<GameManager>();

            FindObjectOfType<MainSceneManager>().CharacterPerformClimbing(PlayerName);
        }

        public void ProcessOpenDoor()
        {
            if(!m_canOpenDoor) { return; }

            FindObjectOfType<CaveManager>().OpenDoor();
        }

        // 2022-05-06 by 楊
        // デバッグ用
        public void OnDieDebugTest()
        {
            Die();
        }

        public void OnGetHitDebugTest()
        {
            StartCoroutine(GetHit());
        }

        protected void LateUpdate()
        {
            CameraRotation();
        }

        private void ProcessChangeCharacter() 
        {
            string sceneName = SceneManager.GetActiveScene().name;
            GameManager gameManager = FindObjectOfType<GameManager>();

            if((sceneName.Equals("CaveScene") && gameManager.IsOnlyYoungerBrotherAction()) || m_isCrouching) { return; }

            if (_input.changeCharacter && !m_isCharacterChanging && _canChangeCharacterFlag)
            {
                m_isCharacterChanging = true;
                switch (PlayerName)
                {
                    case "ElderBrother":
                        if (sceneName.Equals("CaveScene"))
                        {
                            FindObjectOfType<CaveManager>().ChangeControlPlayer(CaveManager.ControlCharacter.YoungerBrother);
                        }
                        else
                        {
                            FindObjectOfType<MainSceneManager>().ChangeControlPlayer(MainSceneManager.ControlCharacter.YoungerBrother);
                        }
                        FindObjectOfType<GameManager>().ChangeControlPlayerName(GameManager.ControlCharacter.YoungerBrother);
                        break;
                    case "YoungerBrother":
                        if (sceneName.Equals("CaveScene"))
                        {
                            FindObjectOfType<CaveManager>().ChangeControlPlayer(CaveManager.ControlCharacter.ElderBrother);
                        }
                        else
                        {
                            FindObjectOfType<MainSceneManager>().ChangeControlPlayer(MainSceneManager.ControlCharacter.ElderBrother);
                        }
                        FindObjectOfType<GameManager>().ChangeControlPlayerName(GameManager.ControlCharacter.ElderBrother);
                        break;
                }
                StartCoroutine(CoolDownChangeCharacterTime());
            }
        }

        public void ProcessPauseGame()
        {
            string sceneName = SceneManager.GetActiveScene().name;
            if (sceneName.Equals("CaveScene"))
            {
                FindObjectOfType<CaveManager>().PauseGame();
            }
            else
            {
                FindObjectOfType<MainSceneManager>().PauseGame();
            }
        }

        public void ProcessActivateTrap()
        {
            if (_input.activateTrap && _canActivateTrap && PlayerName == "ElderBrother")
            {
                _animator.SetTrigger("Command");
                FindObjectOfType<AnimalCharacterAI>().OrderToActivateTrap();
            }
        }

        private void ProcessAttackEnemy()
        {
            if (_input.attackEnemy && _canAttackEnemy && PlayerName == "ElderBrother")
            {
                _animator.SetTrigger("Command");
                FindObjectOfType<AnimalCharacterAI>().AttackEnemy();
            }
        }

        public void AfterCommand()
        {
            _animator.SetTrigger("AfterCommand");
        }

        private void ProcessUseItem()
        {
            if (_canUseItem)
            {
                FindObjectOfType<GameManager>().ShowUseItemHint();
            }
            else
            {
                FindObjectOfType<GameManager>().HideUseItemHint();
            }

            if (FindObjectOfType<GameManager>().CanUseLadder() && _canUseItem && _input.useItem)
            {
                FindObjectOfType<GameManager>().UseLadderItem();
            }
        }

        public void ProcessAttractEnemy()
        {
            if (_canAttractEnemy && PlayerName == "ElderBrother")
            {
                if (!FindObjectOfType<AnimalCharacterAI>().IsAttractingEnemies())
                {
                    _animator.SetTrigger("Command");
                    FindObjectOfType<AnimalCharacterAI>().StartOfAttractingEnemies();
                }
                else
                {
                    FindObjectOfType<AnimalCharacterAI>().EndOfAttractingEnemies();
                    foreach (Enemy e in FindObjectsOfType<Enemy>())
                    {
                        if (e.IsAttractedByBird())
                        {
                            e.StopAttractedByBird();
                        }
                    }
                }
            }
        }

        public void ProcessAttractEnd()
        {
            if (_canAttractEnemy && PlayerName == "ElderBrother")
            {
                if (FindObjectOfType<AnimalCharacterAI>().IsAttractingEnemies())
                {
                    FindObjectOfType<AnimalCharacterAI>().StartOfAttractingEnemies();
                    foreach (Enemy e in FindObjectsOfType<Enemy>())
                    {
                        if (e.IsAttractedByBird())
                        {
                            e.StopAttractedByBird();
                        }
                    }
                }
            }
        }

        public void ProcessChangeScene()
        {
            if (m_canChangeScene)
            {
                m_canChangeScene = false;
                string currentSceneName = SceneManager.GetActiveScene().name;
                if (currentSceneName.Equals("MainScene"))
                {
                    FindObjectOfType<MainSceneManager>().EnterCave();
                }
                else if (currentSceneName.Equals("CaveScene"))
                {
                    FindObjectOfType<CaveManager>().ExitCave();
                }
            }
        }

        private IEnumerator CoolDownChangeCharacterTime()
        {
            yield return new WaitForSeconds(changeCharacterCDTime);
            m_isCharacterChanging = false;
        }

        public void EnableAllDirectionMovementFlag()
        {
            _canHideCharacterFlag = true;
        }

        public void DisbbleAllDirectionMovementFlag()
        {
            _canHideCharacterFlag = false;
        }

        protected void AssignAnimationIDs()
        {
            _animIDSpeed = Animator.StringToHash("Speed");
            _animIDGrounded = Animator.StringToHash("Grounded");
            _animIDJump = Animator.StringToHash("Jump");
            _animIDFreeFall = Animator.StringToHash("FreeFall");
            _animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
            _animIDCrouchWalk = Animator.StringToHash("CrouchWalkSpeed");
        }

        protected void GroundedCheck()
        {
            // set sphere position, with offset
            Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset,
                transform.position.z);
            Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers,
                QueryTriggerInteraction.Ignore);

            // update animator if using character
            if (_hasAnimator)
            {
                _animator.SetBool(_animIDGrounded, Grounded);
            }
        }

        public void SetCameraYaw()
        {
            GameManager gameManager = FindObjectOfType<GameManager>();
            _cinemachineTargetYaw = gameManager.IsInBackMountain() ? CinemachineCameraTarget.transform.rotation.eulerAngles.y
                : CinemachineCameraTarget2.transform.rotation.eulerAngles.y;
        }

        protected void CameraRotation()
        {
            // if there is an input and camera position is not fixed
            if (_input.look.sqrMagnitude >= _threshold && !LockCameraPosition)
            {
                //Don't multiply mouse input by Time.deltaTime;
                //float deltaTimeMultiplier = IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;
                float deltaTimeMultiplier = Time.deltaTime;

                _cinemachineTargetYaw += _input.look.x * deltaTimeMultiplier;
                _cinemachineTargetPitch += _input.look.y * deltaTimeMultiplier;
            }

            // clamp our rotations so our values are limited 360 degrees
            _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
            _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

            // Cinemachine will follow this target
            GameManager gameManager = FindObjectOfType<GameManager>();
            if (gameManager.IsInBackMountain())
            {
                if (PlayerName.Equals("ElderBrother"))
                {
                    CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch,
                        _cinemachineTargetYaw - CameraAngleOverride, 0.0f);
                    partnerCharacter.CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch,
                        -_cinemachineTargetYaw - CameraAngleOverride, 0.0f);
                }
                else
                {
                    CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch,
                        -_cinemachineTargetYaw - CameraAngleOverride, 0.0f);
                    partnerCharacter.CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch,
                        _cinemachineTargetYaw - CameraAngleOverride, 0.0f);
                }
            }
            else
            {
                if (PlayerName.Equals("ElderBrother"))
                {
                    partnerCharacter.CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch,
                        -_cinemachineTargetYaw - CameraAngleOverride, 0.0f);
                }
                CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch,
                    _cinemachineTargetYaw + CameraAngleOverride, 0.0f);
            }
        }

        public void OverrideCameraAngle(float angle)
        {
            CameraAngleOverride = angle;
        }

        public void ProcessCrouch()
        {
            m_isCrouching = true;
            _animator.SetBool("Crouch", true);
            if (!m_isInSoloAction)
            {
                StartCoroutine(OrderAIToCrouch());
            }
        }

        private IEnumerator OrderAIToCrouch()
        {
            yield return new WaitForSeconds(aiCrouchDelayTime);
            if(gameObject.name.Equals("ElderBrother"))
            {
                GameObject.Find("YoungerBrother").GetComponent<FollowCharacterAI>().ProcessCrouch();
            }
            else
            {
                GameObject.Find("ElderBrother").GetComponent<FollowCharacterAI>().ProcessCrouch();
            }
        }

        public void ProcessStandUp()
        {
            m_isCrouching = false;
            _animator.SetBool("Crouch", false);
            StartCoroutine(OrderAIToStandUp());
        }

        private IEnumerator OrderAIToStandUp()
        {
            yield return new WaitForSeconds(aiStandUpDelayTime);
            if (gameObject.name.Equals("ElderBrother"))
            {
                GameObject.Find("YoungerBrother").GetComponent<FollowCharacterAI>().ProcessStandUp();
            }
            else
            {
                GameObject.Find("ElderBrother").GetComponent<FollowCharacterAI>().ProcessStandUp();
            }
        }

        protected void Move()
        {
            // 2022-06-22 by 楊
            // 隠れるところ以外にキャラクターは前後移動できない
            // (アニメーションもプレイしない)
            /*
            Debug.Log("_canHideCharacterFlag: " + _canHideCharacterFlag);
            Debug.Log("_input.move.y: " + _input.move.y);
            */
            if (!_canHideCharacterFlag && _input.move.y != 0.0f) { return; }

            // 2022-11-5 by 楊
            // 「← / →」 : 普通に歩く
            // 「← / →」 ＋ 「Left Shift」: 走る
            // 「← / →」 ＋ 「Left Ctrl」: ゆっくり歩く
            m_isRunning = false;
            m_isStealthWalking = false;
            m_isWalking = false;
            if (m_isCrouching)
            {
                targetSpeed = CrouchSpeed;
            }
            else if (_input.slowDown)
            {
                targetSpeed = SlowDownSpeed;
                if (_input.move != Vector2.zero)
                {
                    m_isStealthWalking = true;
                }
            }
            else if (_input.move != Vector2.zero)
            {
                targetSpeed = SprintSpeed;
                m_isWalking = true;
            }

            // a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

            // note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
            // if there is no input, set the target speed to 0
            if (_input.move == Vector2.zero) targetSpeed = 0.0f;

            // a reference to the players current horizontal velocity
            float currentHorizontalSpeed = new Vector3(_controller.velocity.x, 0.0f, _controller.velocity.z).magnitude;

            float speedOffset = 0.1f;
            float inputMagnitude = _input.analogMovement ? _input.move.magnitude : 1f;
            if (!_canHideCharacterFlag)
            {
                inputMagnitude = (_input.move.y != 0.0f) ? 0 : inputMagnitude;
            }

            // accelerate or decelerate to target speed
            if (currentHorizontalSpeed < targetSpeed - speedOffset ||
                currentHorizontalSpeed > targetSpeed + speedOffset)
            {
                // creates curved result rather than a linear one giving a more organic speed change
                // note T in Lerp is clamped, so we don't need to clamp our speed
                _speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude,
                    Time.deltaTime * SpeedChangeRate);

                // round speed to 3 decimal places
                _speed = Mathf.Round(_speed * 1000f) / 1000f;
            }
            else
            {
                _speed = targetSpeed;
            }

            _animationBlend = Mathf.Lerp(_animationBlend, targetSpeed, Time.deltaTime * SpeedChangeRate);
            if (_animationBlend < 0.01f) _animationBlend = 0f;

            // normalise input direction
            Vector3 inputDirection = new Vector3(_input.move.x, 0.0f, _input.move.y).normalized;

            // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
            // if there is a move input rotate player when the player is moving
            if (_input.move != Vector2.zero)
            {
                _targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg +
                                  _mainCamera.transform.eulerAngles.y;
                float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, _targetRotation, ref _rotationVelocity,
                    RotationSmoothTime);

                // rotate to face input direction relative to camera position
                transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
            }


            Vector3 targetDirection = Quaternion.Euler(0.0f, _targetRotation, 0.0f) * Vector3.forward;

            // move the player
            _controller.Move(targetDirection.normalized * (_speed * Time.deltaTime) +
                                new Vector3(0.0f, _verticalVelocity, 0.0f) * Time.deltaTime);


            // update animator if using character
            if (m_isCrouching)
            {
                _animator.SetFloat(_animIDCrouchWalk, targetSpeed);
            }
            else
            {
                if (_hasAnimator)
                {
                    _animator.SetFloat(_animIDSpeed, _animationBlend);
                    _animator.SetFloat(_animIDMotionSpeed, inputMagnitude);
                }
            }
        }


        public void ActivatePickItemFlag()
        {
            _canPickItem = true;
            //Debug.Log("ActivatePickItemFlag - _canPickItem: " + _canPickItem);
        }

        public void DeactivatePickItemFlag()
        {
            _canPickItem = false;
        }

        private void ProcessPickUpItem()
        {
            //Debug.Log("ProcessPickUpItem - _canPickItem: " + _canPickItem);
            if (_input.pickUp && _canPickItem && PlayerName == "ElderBrother" &&
                FindObjectOfType<AnimalCharacterAI>().HasTargetItem() && !FindObjectOfType<AnimalCharacterAI>().IsItemPicked())
            {
                _canPickItem = false;
                FindObjectOfType<AnimalCharacterAI>().OrderToTakeItem();
            }
        }

        public void SoloAction()
        {
            GameObject ElderBrother = GameObject.Find("ElderBrother");
            GameObject Bird = GameObject.Find("Anqa");
            FollowCharacterAI ElderBrotherFollowCharacterAI = ElderBrother.GetComponent<FollowCharacterAI>();
            ElderBrotherFollowCharacterAI.StartIdleing();
            ElderBrotherFollowCharacterAI.enabled = false;
            ElderBrother.GetComponent<NavMeshAgent>().enabled = false;
            Bird.GetComponent<AnimalCharacterAI>().enabled = false;
            Bird.GetComponent<NavMeshAgent>().enabled = false;
            m_isInSoloAction = true;
            FindObjectOfType<GameManager>().SetOnlyYoungerBrotherAction(true);
        }

        public void Conjuction()
        {
            Debug.Log("Conjuction TEST");
            GameObject ElderBrother = GameObject.Find("ElderBrother");
            GameObject Bird = GameObject.Find("Anqa");
            ElderBrother.GetComponent<FollowCharacterAI>().enabled = true;
            ElderBrother.GetComponent<NavMeshAgent>().enabled = true;
            Bird.GetComponent<AnimalCharacterAI>().enabled = true;
            Bird.GetComponent<NavMeshAgent>().enabled = true;
            m_isInSoloAction = false;
            FindObjectOfType<GameManager>().SetOnlyYoungerBrotherAction(false);
        }

        public bool IsWalking()
        {
            return m_isWalking;
        }

        public bool IsRunning()
        {
            return m_isRunning;
        }

        public bool IsStealthWalking()
        {
            return m_isStealthWalking;
        }

        public void EnablePlayerAnimatorController()
        {
            _animator.runtimeAnimatorController = animatorControllerPlayer;
            if (m_isCrouching)
            {
                _animator.SetBool("Crouch", true);
            }
        }


        public bool CanSoloAction()
        {
            return _canSoloAction == true;
        }

        public bool CanConjunction()
        {
            return _canConjunction == true;
        }

        public bool IsInSoloAction()
        {
            return m_isInSoloAction == true;
        }

        public void SetToSoloAction()
        {
            if (PlayerName.Equals("YoungerBrother"))
            {
                m_isInSoloAction = true;
            }
        }

        public void EnableSoloAction()
        {
            _canSoloAction = true;
        }

        public void DisableSoloAction()
        {
            _canSoloAction = false;
        }

        public void EnableConjuction()
        {
            _canConjunction = true;
        }

        public void DisableConjuction()
        {
            _canConjunction = false;
        }

        // 2022-05-10 by 楊
        // AI移動スピードを一致させる
        public float GetCurrentTargetSpeed()
        {
            return targetSpeed;
        }

        // 2022-04-28 by 楊
        // キャラクター変更する時、元の操作キャラクターに全て実行しているアニメーションを停止させる
        public void StopAnimation()
        {
            _animator.SetFloat(_animIDSpeed, 0);
            _animator.SetFloat(_animIDMotionSpeed, 0);
        }

        protected void JumpAndGravity()
        {
            if (m_isCrouching) { return ; }

            if (Grounded)
            {
                // reset the fall timeout timer
                _fallTimeoutDelta = FallTimeout;

                // update animator if using character
                if (_hasAnimator)
                {
                    _animator.SetBool(_animIDJump, false);
                    _animator.SetBool(_animIDFreeFall, false);
                }

                // stop our velocity dropping infinitely when grounded
                if (_verticalVelocity < 0.0f)
                {
                    _verticalVelocity = -2f;
                }

                // Jump
                if (_input.jump && _canJump && _jumpTimeoutDelta <= 0.0f)
                {
                    // the square root of H * -2 * G = how much velocity needed to reach desired height
                    _verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);

                    // update animator if using character
                    if (_hasAnimator)
                    {
                        _animator.SetBool(_animIDJump, true);
                    }
                }

                // jump timeout
                if (_jumpTimeoutDelta >= 0.0f)
                {
                    _jumpTimeoutDelta -= Time.deltaTime;
                }
                _canChangeCharacterFlag = true;
            }
            else
            {
                // reset the jump timeout timer
                _jumpTimeoutDelta = JumpTimeout;

                // fall timeout
                if (_fallTimeoutDelta >= 0.0f)
                {
                    _fallTimeoutDelta -= Time.deltaTime;
                }
                else
                {
                    // update animator if using character
                    if (_hasAnimator)
                    {
                        _animator.SetBool(_animIDFreeFall, true);
                    }
                }

                // if we are not grounded, do not jump
                _input.jump = false;
                _canChangeCharacterFlag = false;
            }

            // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
            if (_verticalVelocity < _terminalVelocity)
            {
                _verticalVelocity += Gravity * Time.deltaTime;
            }
        }

        protected static float ClampAngle(float lfAngle, float lfMin, float lfMax)
        {
            if (lfAngle < -360f) lfAngle += 360f;
            if (lfAngle > 360f) lfAngle -= 360f;
            return Mathf.Clamp(lfAngle, lfMin, lfMax);
        }

        protected void OnDrawGizmosSelected()
        {
            Color transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
            Color transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

            if (Grounded) Gizmos.color = transparentGreen;
            else Gizmos.color = transparentRed;

            // when selected, draw a gizmo in the position of, and matching radius of, the grounded collider
            Gizmos.DrawSphere(
                new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z),
                GroundedRadius);
        }

        protected void OnFootstep(AnimationEvent animationEvent)
        {
            if (animationEvent.animatorClipInfo.weight > 0.5f)
            {
                if (FootstepAudioClips.Length > 0)
                {
                    var index = Random.Range(0, FootstepAudioClips.Length);
                    if (FootstepAudioClips[index] != null && transform != null && FootstepAudioVolume > 0f)
                    {
                        AudioSource.PlayClipAtPoint(FootstepAudioClips[index], transform.TransformPoint(_controller.center), FootstepAudioVolume);
                    }
                    if (!characterAudioSource.isPlaying)
                    {
                        characterAudioSource.PlayOneShot(FootstepAudioClips[index]);
                    }
                }
            }
        }

        protected void OnLand(AnimationEvent animationEvent)
        {
            if (animationEvent.animatorClipInfo.weight > 0.5f)
            {
                if (!characterAudioSource.isPlaying)
                {
                    //AudioSource.PlayClipAtPoint(LandingAudioClip, transform.TransformPoint(_controller.center), FootstepAudioVolume);
                    characterAudioSource.PlayOneShot(LandingAudioClip);
                }
            }
        }

        private IEnumerator GetHit()
        {
            _canControl = false;
            _animator.SetTrigger("GetHit");
            yield return new WaitForSeconds(GetHitSeconds);
            _animator.SetTrigger("AfterGetHit");
            _canControl = true;
        }

        public void Die()
        {
            _canControl = false;
            _animator.SetTrigger("Die");
            FindObjectOfType<MainSceneManager>().GameOver();
        }
        
        public void EnableJump()
        {
            _canJump = true;
        }

        public void DisableJump()
        {
            _canJump = false;
        }

        public bool IsJumping()
        {
            return _canJump;
        }

        public void EnableClimbing()
        {
            m_canClimb = true;
        }

        public void DisableClimbing()
        {
            m_canClimb = false;
        }

        public void EnableOpenDoor()
        {
            m_canOpenDoor = true;
        }

        public void DisableOpenDoor()
        {
            m_canOpenDoor = false;
        }

        public void SetTargetLadder(GameObject ladder)
        {
            targetLadder = ladder;
        }

        public void UnsetTargetLadder()
        {
            transform.LookAt(null);
        }

        public void EnableControl()
        {
            _canControl = true;
        }

        public void DisableControl()
        {
            _canControl = false;
        }

        public void EnableActivateTrap()
        {
            _canActivateTrap = true;
        }

        public void DisableActivateTrap()
        {
            _canActivateTrap = false;
        }

        public bool CanActiviateTrap()
        {
            return _canActivateTrap;
        }

        public void EnableAttackEnemyFlag()
        {
            _canAttackEnemy = true;
            FindObjectOfType<GameManager>().ShowAttackEnemyHint();
        }

        public void EnableUseItemFlag()
        {
            _canUseItem = true;
        }

        public void DisableUseItemFlag()
        {
            _canUseItem = false;
        }

        public void EnableAttractEnemyFlag()
        {
            _canAttractEnemy = true;
            if(FindObjectOfType<AnimalCharacterAI>().IsAttractingEnemies())
            {
                FindObjectOfType<GameManager>().ShowAttractEnemyEndHint();
                FindObjectOfType<GameManager>().HideAttractEnemyHint();
            }
            else
            {
                FindObjectOfType<GameManager>().ShowAttractEnemyHint();
                FindObjectOfType<GameManager>().HideAttractEnemyEndHint();
            }
        }

        public void DisableAttractEnemyFlag()
        {
            _canAttractEnemy = false;
            FindObjectOfType<GameManager>().HideAttractEnemyHint();
            FindObjectOfType<GameManager>().HideAttractEnemyEndHint();
        }

        public void DisableAttackEnemyFlag()
        {
            _canAttackEnemy = false;
            if(!_canActivateTrap || !m_canClimb)
            {
                FindObjectOfType<GameManager>().HideAttackEnemyHint();
            }
        }

        public void EnableChangeSceneFlag()
        {
            m_canChangeScene = true;
        }

        public void DisableChangeSceneFlag()
        {
            m_canChangeScene = false;
        }

        public void EnableJumpedToGroundFlag()
        {
            m_JumpedToGround = true;
        }

        public void DisableJumpedToGroundFlag()
        {
            m_JumpedToGround = false;
        }

        public bool IsJumpedToGround()
        {
            return m_JumpedToGround;
        }

        public void ActvivateCannotStandUpFlag()
        {
            _cannotStandUpFlag = true;
        }

        public void DeactivateCannotStandUpFlag()
        {
            _cannotStandUpFlag = false;
        }

        public bool IsCrouching()
        {
            return m_isCrouching == true;
        }

        public bool CanStandUp()
        {
            return _cannotStandUpFlag != true;
        }

        private void OnCollisionEnter(Collision collider)
        {
            Debug.Log("Thrid Person Controller OnCollision Enter collider: " + collider.gameObject.name);
            if (collider.gameObject.tag.Equals("Enemy"))
            {
                collider.gameObject.GetComponent<Enemy>().Stop();
                DisableControl();
                StopAnimation();
                FindObjectOfType<MainSceneManager>().GameOver();
            }
        }
    }
}